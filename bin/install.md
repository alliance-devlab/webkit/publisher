#! /bin/bash


```bash
[[ $COLORTERM =~ ^(truecolor|24bit)$ ]]
#? Test Terminal for Color before running. Add to capabilities list.

#? Display Ascii Art:
ASCII_ART='https://gitlab.com/alliance-devlab/grav-skeleton-alliance/-/raw/main/assets/shell/install-art.ascii'

#? 'https://ipfs.io/ipfs/bafkreigkveei7zxqeevdtwjbmprkhoag2j77kq7urngkndozx5z4dvkrie'
#? 'https://ipfs.runfission.com/ipns/NevadaCountyAssembly.files.fission.name/p/images/IPFS/pheonix.ascii.txt'
display_art () {
    #? curl -s $ASCII_ART_POSTER | cat
    #? curl -s $ASCII_ART_HEADER | cat
    curl -s $ASCII_ART | cat
}

display_art;
```



```fish
#? Attach deploy token:
set USER steward
set TOKEN -ftsJp_8na29zZ_rRJL3
set REPO "https://$USER:$TOKEN@gitlab.com/alliance-devlab/webkit/publisher.git"
```


```bash
# Attach deploy token:
USER=steward
TOKEN=-ftsJp_8na29zZ_rRJL3
REPO="https://$USER:$TOKEN@gitlab.com/alliance-devlab/webkit/publisher.git"
```

```bash
bin=$HOME/.local/bin
lib=$HOME/.local/lib/webkit
config=$HOME/.config/webkit

mkdir -p $bin
mkdir -p $lib
mkdir -p $config
```


### Install git:
```
which git
sudo apt install git
```

### Move to local bin:
```cd ~/.local/bin```

# Install 'yq':
```bash
curl -Lso ./yq https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64

sudo chmod a+x ./yq
```

<!--
# Install gpm:
```bash
curl -Ls https://github.com/aerys/gpm-packages/raw/master/gpm-linux64/gpm-linux64.tar.gz | tar xvz > /dev/null
-->


