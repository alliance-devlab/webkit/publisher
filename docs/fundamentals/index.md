Webkit is both simple and powerful. To learn to use Webkit, follow these guides:

### [Organizing Webkit Content](1.organizing_content.md)

##### **Webkit _content_** is _styled_ **markdown** that is _injected_ with data, _embedded_ with media, and _distributed_ on Web3 infrastructure.

### [Writing Markdown++](2.writing_markdown.md)

##### **Markdown++** is a special notation for writing Webkit content, which is designed to be expressive and clean, so that configuration stays out of the way of the creative process.
