
# Create system user for daemon service

`sudo useradd -r -s /bin/null webkit`
Note: will show warning: `missing or non-executable shell '/bin/null'`

By default the command useradd doesn't create home directories, but for a daemon I recommend you to use the system option and change the shell to a non-existent one so no one can login with said account (in ssh for example):


# "Jailing" the service (system user), [chroot](https://www.cyberciti.biz/faq/unix-linux-chroot-command-examples-usage-syntax/)

See: ["Principle of least privilege"](https://en.wikipedia.org/wiki/Principle_of_least_privilege)

[su: Running Command as Another User](https://tecadmin.net/linux-run-commands-as-another-user/)



# MacOS launchd 
[node-launchd]https://www.npmjs.com/package/node-launchd




[How to Run Node.js as background Service](https://stackoverflow.com/questions/4018154/how-do-i-run-a-node-js-app-as-a-background-service)

Create `webkit.service`:
```
[Unit]
Description=Webkit Login Service

[Service]
ExecStart=/home/steward/.local/lib/webkit/publisher/account/auth
Restart=always
User=nobody
# Note Debian/Ubuntu uses 'nogroup', RHEL/Fedora uses 'nobody'
Group=nogroup
Environment=PATH=/usr/bin:/usr/local/bin
Environment=NODE_ENV=production
WorkingDirectory=/home/steward/.local/lib/webkit

[Install]
WantedBy=multi-user.target
```

Copy your service file into:
`/etc/systemd/system`

Start:
`systemctl enable webkit`

View logs:
`journalctl -u webkit`


Note that it's also possible to run systemd services as a user. See for example [this tutorial](https://wiki.archlinux.org/title/Systemd/User).

You can put your service file in:
`~/.config/systemd/user`

Start:
`systemctl --user start myapp`

Enable:
`systemctl --user enable myapp`
