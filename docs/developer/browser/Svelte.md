
[Svelte Docs](https://svelte.dev/docs)


[Module context / Sharing code](https://svelte.dev/tutorial/sharing-code)






### [Svelte Material UI tutorial (SMUI)](https://www.youtube.com/watch?v=OyjZ7dezADw)

[Installation](https://sveltematerialui.com/INSTALL.md)

[Theming SMUI](https://sveltematerialui.com/THEMING.md)

[typography](https://sveltematerialui.com/demo/typography/)

