## Material Design:
[Material Design Typography](https://material.io/design/typography/understanding-typography.html#applying-the-type-scale)



### [Svelte Material UI tutorial (SMUI)](https://www.youtube.com/watch?v=OyjZ7dezADw)

[Installation](https://sveltematerialui.com/INSTALL.md)

[Theming SMUI](https://sveltematerialui.com/THEMING.md)

[typography](https://sveltematerialui.com/demo/typography/)

