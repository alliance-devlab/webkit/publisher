
[Web Workers API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers)


[Comlink package](https://www.npmjs.com/package/comlink)



[!Issue: WebWorkers + Svelte](https://github.com/sveltejs/svelte/issues/4545)

 * Rollup and `rollup-plugin-off-main-thread`
 * Create Svelte store in WebWorker
 * [Comlink](https://github.com/GoogleChromeLabs/comlink) to "expose" the Svelte store via an underlying Proxy object.
 * Load WebWorker into Svelte
