

## Gitlab Files API:

# Get Raw File (token, project_id, file_path):
```shell
TOKEN=glpat-bigBe6YxNGTQqojdxeZQ
project=35611374
file_path="bin%2Finstall.sh" #? bin/install.sh (URI-encoded)

curl --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/$project/repository/files/$file_path/raw"
```
