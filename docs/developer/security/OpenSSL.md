
### Setting up Mutual Authentication:
 * Generate Certificate Authority
      1. Generate CA Key
      2. Generate CA Certificate
      3. Add CA Cert to Local Trust.

 * Generate Client Credentials
      1. Generate Client Key
      2. Generate Client Cert
      3. Generate .p12
      4. Transform .p12 into `client.key.pem` and `client.cert.pem`

[Remove secrets from pkcs12 certificate](https://blog.armbruster-it.de/2010/03/remove-the-passphrase-from-a-pkcs12-certificate/)





[Installing CA Certificate into Trust Store (Ubuntu/Debian)](https://ubuntu.com/server/docs/security-trust-store)










## Related Tagents:

   * [Use `easyrsa` as CA](https://www.digitalocean.com/community/tutorials/how-to-set-up-and-configure-a-certificate-authority-ca-on-ubuntu-20-04)