

### SHA-1 Deprecated.

[SHA1 is Fully and Practically Broken](https://sha-mbles.github.io/)
UPDATE--SHA-1, the 25-year-old hash function designed by the NSA and considered unsafe for most uses for the last 15 years, has now been “fully and practically broken” by a team that has developed a chosen-prefix collision for it.

The development means that **an attacker could essentially impersonate another person** by creating a PGP key that’s identical to the victim’s key. The technique that the researchers developed is quite complex and required two months of computations on 900 individual GPUs, so it is by no means a layup for most adversaries. **SHA-1 has been phased out of use** in most applications and none of the major browsers will accept certificates signed with SHA-1.

