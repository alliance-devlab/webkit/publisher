## [OpenPGP & GnuPG](https://askubuntu.com/questions/186805/difference-between-pgp-and-gpg)

**Pretty Good Privacy** (PGP) was originally written by Phil Zimmermann, and is now owned by Symantec. The formats for keys, encrypted messages and message signatures were defined by that software. These have now been formalised as the **OpenPGP standard**.

The **GNU Privacy Guard**, GnuGP (GPG) software is an independent implementation of the OpenPGP standards, so you can use it to exchange encrypted messages with people using other OpenPGP implementations . . 

Due to GnuGP's popularity on Linux systems, it is also fairly common for people to incorrectly use the term "GPG" to refer to the whole OpenPGP cryptography system (e.g. "GPG keys" or "GPG signatures"). It is usually pretty clear what they mean from the context though.


## OpenPGP 

   * Node package: ['openpgp'](https://www.npmjs.com/package/openpgp)




## GnuPrivacyGuard

See this exceptional [tutorial on gpg](https://linux.101hacks.com/unix/gpg-command-examples/).

[GPG Configuration Options](https://www.gnupg.org/documentation/manuals/gnupg-devel/GPG-Configuration-Options.html)

> Anyway, you should never pass the passphrase as parameter (you're using the wrong argument anyway, have a look at the --passphrase... options in the GnuPG man page): all other users on the system can read all processes' command lines including the secret passphrase! Instead, use `--passphrase-fd 0` and attach your application to GnuPG's STDIN (like you'd do with a pipe on the command line), and pass the passphrase as input.


### [What are Subkeys?](https://wiki.debian.org/Subkeys)



### [Configuring GPG](https://raw.githubusercontent.com/bfrg/gpg-guide/master/gpg-agent.conf)
[GPG Manual: gpgconf](https://www.gnupg.org/documentation/manuals/gnupg/Invoking-gpgconf.html)

GPG uses a 'pinentry' program that is responsible for passing passphrases to GPG. The default on Ubuntu is the `pinentry-gnome` program available in: `ls /usr/bin | grep pinentry`:

 * pinentry
 * pinentry-curses
 * pinentry-gnome3
 * pinentry-x11

`pinentry-gnome3` is great and integrates perfectly with Gnome Keyring (GUI): 
"implements a PIN entry dialog based on GNOME 3, which aims to follow the GNOME Human Interface Guidelines."

However, `pinentry3-gnome` caches passphrases in ways that are difficult to predict. In order for the `GPG.authenticate` function to work as expected, it is necessary to alter the gpg-agent configuration:

 1. Append option to gpg-agent.conf: `echo "pinentry-program /usr/bin/pinentry-curses" > ~/.gnupg/gpg-agent.conf`
 2. Reload agent: `gpg-connect-agent reloadagent /bye`
 3. Error: ~~Check that options are loaded correctly: `gpgconf --check-config ~/.gnupg/gpg-agent.conf`~~
      - `gpgconf --check-config ~/.gnupg/gpg-agent.conf` is not returning expected results.
         Documentation covers assigning 'pinentry-curses' in ~/.gnupg/gpg-agent.conf
         Appears to be working properly in practice
         Except that `gpgconf --check-config ...` and not listed by `gpgconf --list-options gpg-agent`.
         This does not make sense according to the documentation. 😕

 4. Print `gpg-agent` configuration: `gpgconf --list-options gpg-agent`

This procedure should be integrated into early installation/configuration.










### Simple usage of `gpg`:
```fish
set keys "$HOME/.config/webkit/publisher/"
set uid $USER
set gpg_id "webkit-publisher-$uid"
set pw 'secret'


# Quick Generate Key:
gpg --batch --passphrase $pw --quick-gen-key $gpg_id

# Quick Generate Key (safely):
echo $pw | gpg --batch --passphrase-fd 0 --quick-gen-key $gpg_id


# Select Fingerprint of Public Key:
set fingerprint (string trim  $(gpg --list-keys | head -4 | tail -1))
gpg --fingerprint $gpg_id

# Set to never expire:
gpg --quick-set-expire $fingerprint never

# List keys:
gpg --list-keys
gpg --list-secret-keys

# List key:
gpg --list-keys $gpg_id
gpg --list-secret-keys $gpg_id


# Encrypt/Decrypt private file:
gpg --encrypt --sign --armor -r $gpg_id $file
gpg --decrypt $file

# (--passphrase-file)
gpg -d --pinentry-mode loopback --passphrase-file pass.txt < encrypted.asc

echo $pw | gpg --batch --passphrase-fd 0 --output private.pgp --armor --export-secret-key steward@webkit.ipfs


   # without interaction (pin-entry mode):
   gpg --pinentry-mode loopback --passphrase=$secret --encrypt --sign --armor -r $gpg_id $filename
   gpg --decrypt joke.txt.asc > /dev/null 2>&1
   echo $pw | gpg --batch --passphrase-fd 0 --decrypt $file


# Export keys:
gpg --output public.asc.pgp --export $gpg_id
gpg --output private.asc.pgp --export-secret-key $gpg_id

   # Export ASCII-encrypted keys:
   gpg --output public.asc.pgp --armor --export $gpg_id
   gpg --output private.asc.pgp --armor --export-secret-key $gpg_id

   # Export keys without interaction (safely):
   echo $pw | gpg --batch --passphrase-fd 0 --output public.pgp --armor --export $gpg_id
   echo $pw | gpg --batch --passphrase-fd 0 --output private.pgp --armor --export-secret-key $gpg_id

   # Export keys without interaction (pin-entry mode):
   gpg --pinentry-mode loopback --passphrase=$pw --output public.pgp --armor --export $gpg_id
   gpg --pinentry-mode loopback --passphrase=$pw --output private.pgp --armor --export-secret-key $gpg_id

# Import friend's public keys:
gpg --import friend.pgp

# Send friend an encrypted file:
gpg --recipient friend --encrypt filename

# Send friend an ASCII-encrypted file:
gpg --recipient bob --armor --encrypt filename

# Sign Git commit:
git add <path/to/file>
git commit --gpg-sign --signoff -m 'commit message'

git log --show-signature -1
```fish
commit ea26afb7d635a615547490e05a7aef2d9bcda265
gpg: Signature made Tue 28 Nov 2017 11:15:12 AM AEST
gpg:                using RSA key 0FAA11C1B55BFA62
gpg: Good signature from "Steward Commons <steward@webkit.ipfs>" [unknown]
Primary key fingerprint: DBE2 4D9E 8ECC 5B29 5F33  FF61 A468 00C5 D9A8 855E
     Subkey fingerprint: 3E59 553C 2748 4079 C1A1  5DC8 0FAA 11C1 B55B FA62
Author: Anil Belur <abelur@linux.com>
Date:   Tue Nov 28 10:45:29 2017 +1000
```


# Import keys:
gpg --import public.pgp; gpg --import private.pgp
  # Trust private key "ultimately":

### [Trust the keys noninteractive in batch](https://raymii.org/s/articles/GPG_noninteractive_batch_sign_trust_and_send_gnupg_keys.html)

```bash
echo -e "5\ny\n" |  gpg --command-fd 0 --expert --edit-key $fingerprint trust quit;
```


//?
expect -c "spawn gpg --edit-key $fingerprint trust quit; send \"never\"; expect eof"

//? edit key as script?
gpg --command-fd=0 --status-fd=1 --expert --edit-key

# Delete keys:
gpg --delete-secret-key $gpg_id
gpg --delete-key $gpg_id





# Encrypt/Decrypt file
   # (manual passphrase only):
      #? Encrypt file:
      gpg -c test.txt
      rm test.txt
      #? Decrypt file:
      gpg test.txt
      cat test.txt

   # (pinentry mode, exported key + pw):
      gpg --pinentry-mode loopback --passphrase=$pw ./path/to/private.pgp



```









```bash
```

## Script: Generate publisher GPG keys:
```fish
set keys "$HOME/.config/webkit/publisher/"
set uid 'steward'
set gpg_id "webkit-publisher-$uid"
set pw 'secret'

mkdir -p $keys; cd $keys

echo $pw | gpg --batch --passphrase-fd 0 --quick-gen-key $gpg_id
echo $pw | gpg --batch --passphrase-fd 0 --output public.pgp --armor --export $gpg_id

gpg --pinentry-mode loopback --passphrase=$secret --output public.pgp --armor --export $gpg_id
gpg --pinentry-mode loopback --passphrase=$secret --output private.pgp --armor --export-secret-key $gpg_id
```




## [pass](https://www.passwordstore.org/)

## script:
```fish
# Generate Webkit GPG Keypair:
gpg --batch --passphrase '' --quick-gen-key webkit

# Set expiration to never:
set fingerprint (string trim  $(gpg --list-keys | head -4 | tail -1))
gpg --quick-set-expire $fingerprint never

#* Initialize Pass:
pass init -p webkit webkit

#* Export keys:
mkdir -p ~/.config/webkit/keys
gpg --output ~/.config/webkit/keys/public.pgp --armor --export webkit
gpg --output ~/.config/webkit/keys/private.pgp --armor --export-secret-key webkit
```


### GPG Trust levels

GPG trust levels are confusing.

  1 = I don't know or won't say
  2 = I do NOT trust
  3 = I trust marginally
  4 = I trust fully
  5 = I trust ultimately
  m = back to the main menu

let = [null, false, 'marginally', 'fully', 'ultimately']

```js
trustIdentity(id, level=null) {
   if (!GPG.authenticate(id)) throw `Not authenticated as ${id}`
   let fingerprint = GPG.searchKeys(id)[0].fingerprint

   const set = (() => {
      switch (level) {
         case null:
            return 1
         case false:
            return 2
         case 'marginally':
            return 3
         case 'fully':
            return 4
         case 'ultimately':
            return 5
      }
   })()



   sh.execSync(`gpg --command-fd 0 --expert --edit-key ${fingerprint}`)


}
```

```fish
gpg --export-ownertrust
echo -e "0\ny\n" | gpg --command-fd 0 --expert --edit-key 46D236ED620B4A203F9C8C6A57A65EB244063087 trust quit;

```