
### Generate PrivateKey/Certificate with OpenSSL:

```

```


### Mutual Authentication:
https://medium.com/@sevcsik/authentication-using-https-client-certificates-3c9d270e8326





### Use OpenSSL to generate:

 * service.key
 * service.cert

 * client.key
 * client.csr
 * client.cert



 ### TLS (Mutual Authentication of API): Axios + Express
```js
const fs = require('fs')
const https = require('https')
const Request = require('axios')

const httpsAgent = new https.Agent({
   cert: fs.readfileSync('client.cert'),
   key: fs.readFileSync('client.key'),
   ca: fs.readFileSync('ca.cert')
})

if (await Request('https://localhost:9500/login/steward', { httpsAgent, data: { nid: 'steward@webkit.ipfs', pw: 'secret' } })) {
   console.log(`Logged in as ${this.activePublisher.name}`)
}
```


### Add the CA certificate to the trusted root certificates:
```bash
sudo apt install -y ca-certifcates

sudo cp service.cert /usr/local/share/ca-certificates
sudo update-ca-certificates
```