
### [Temporary files & directories](https://advancedweb.hu/secure-tempfiles-in-nodejs-without-dependencies/)

Safe use of temporary files for sensitive information:
```js
// fs is promises!
const fs = require("fs").promises;
const os = require("os");
const path = require("path");

const withTempFile = (fn) => withTempDir((dir) => fn(path.join(dir, "file")));

const withTempDir = async (fn) => {
	const dir = await fs.mkdtemp(await fs.realpath(os.tmpdir()) + path.sep);
	try {
		return await fn(dir);
	}finally {
		fs.rmdir(dir, {recursive: true});
	}
};

//or:
const temporaryDirectory = () => fs.mkdtempSync(fs.realpathSync(os.tmpdir()) + Path.sep)
const randomFilename = () => Crypto.randomBytes(3).toString('base64').replace('/', '_')
```

### Restricted directories:





### Which Temp directory to use?
Don’t assume `/tmp` is the place to put tempfiles.

The `os.tmpdir` is better, as it picks up the tmp location in a configurable way. But it seems like there are problems with it too as it can be a symlink in some cases.

Fortunately, Node provides a utility to solve the symlink problem, so combining the two parts, the reliable solution to find out the tempdir location is:

```js
fs.realpath(os.tmpdir())
```


### [Pass sudo password via STDIN](https://www.sudo.ws/man/1.8.16/sudo.man.html)
```bash
echo password | sudo -u root --stdin

#or just:
echo password | sudo -S
```
```js
sh.execSync(`sudo -S ${command}`, { input: 'sudoUserPassword' })
```


### [Sudo Prompt](https://www.npmjs.com/package/sudo-prompt)
