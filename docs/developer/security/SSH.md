
### Generate SSH
```
mkdir ~/.ssh
chmod 0700 ~/.ssh
ssh-keygen -t rsa
```

### Install & configure keychain:
https://www.cyberciti.biz/faq/ubuntu-debian-linux-server-install-keychain-apt-get-command/

[Keychain](https://www.funtoo.org/Funtoo:Keychain) is a convenience wrapper for `ssh-agent`


