
[base64, Binary Strings](https://stackabuse.com/encoding-and-decoding-base64-strings-in-node-js/)


[keystore-idb](https://www.npmjs.com/package/keystore-idb)
(created by: [Daniel Holmgren](https://dholms.github.io/))

**In-browser key management with IndexedDB and the Web Crypto API.**

Securely store and use keys for encryption, decryption, and signatures. IndexedDB and Web Crypto keep keys safe from malicious javascript.

Supports both RSA (RSASSA-PKCS1-v1_5 & RSA-OAEP) and Elliptic Curves (P-256, P-381 & P-521).

ECC (Elliptic Curve Cryptography) is only available on Chrome. Firefox and Safari do not support ECC and must use RSA. Specifically, this is an issue with storing ECC keys in IndexedDB

#### Implimentation(from 'fission-suite/webnative/src/keystore.ts'):
    ```ts
    import keystore from "keystore-idb"
    import RSAKeyStore from "keystore-idb/lib/rsa/keystore.js"
    import { CryptoSystem } from "keystore-idb/lib/types.js"

    const KEYSTORE_CFG = { type: CryptoSystem.RSA }


    let ks: RSAKeyStore | null = null


    export const clear = async (): Promise<void> => {
    ks = await get()
    await ks.destroy()
    ks = null
    }

    export const create = async (): Promise<RSAKeyStore> => {
    return (await keystore.init(KEYSTORE_CFG)) as RSAKeyStore
    }

    export const set = async (userKeystore: RSAKeyStore): Promise<void> => {
    ks = userKeystore
    }

    export const get = async (): Promise<RSAKeyStore> => {
    if (ks) return ks
    ks = (await keystore.init(KEYSTORE_CFG)) as RSAKeyStore
    return ks
    }
    ```




### [Bcoin](https://bcoin.io/#features)

### [An Example of a Signed Document](https://git.gnupg.org/cgi-bin/gitweb.cgi?p=libgcrypt.git;a=blob;f=README)


### NeDB Encryption:
https://stackoverflow.com/questions/42539412/protect-nedb-database-with-user-password

```js
const crypto = require('crypto')
const Datastore = require('nedb')

const ALGORITHM = 'aes-256-cbc'
const BLOCK_SIZE = 16
const KEY_SIZE = 32

// Generate a random key.
// If you want to use a password, use scrypt to generate the key instead.
const key = crypto.randomBytes(KEY_SIZE)

const db = new Datastore({
    filename: 'encrypted.db',

    afterSerialization (plaintext) { //* Encryption

        // Generate random IV.
        const iv = crypto.randomBytes(BLOCK_SIZE)

        // Create cipher from key and IV.
        const cipher = crypto.createCipheriv(ALGORITHM, key, iv)

        // Encrypt record and prepend with IV.
        const ciphertext = Buffer.concat([iv, cipher.update(plaintext), cipher.final()])

        // Encode encrypted record as Base64.
        return ciphertext.toString('base64')
    },

    beforeDeserialization (ciphertext) { //* Decryption

        // Decode encrypted record from Base64.
        const ciphertextBytes = Buffer.from(ciphertext, 'base64')

        // Get IV from initial bytes.
        const iv = ciphertextBytes.slice(0, BLOCK_SIZE)

        // Get encrypted data from remaining bytes.
        const data = ciphertextBytes.slice(BLOCK_SIZE)

        // Create decipher from key and IV.
        const decipher = crypto.createDecipheriv(ALGORITHM, key, iv)

        // Decrypt record.
        const plaintextBytes = Buffer.concat([decipher.update(data), decipher.final()])

        // Encode record as UTF-8.
        return plaintextBytes.toString()
    },
})
```





[keystore-idb package](https://www.npmjs.com/package/keystore-idb)
