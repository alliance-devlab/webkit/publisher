
### Debugging Node from Command Line

Webkit utilizes the 'debug' package for switching the debug console between branches of code.

This is controlled by setting the _environment variable_ `DEBUG`.

If executing the node script from a fish shell:

```fish
# to debug everything:
set -x DEBUG "*"

# to debug specific:
set -x DEBUG "build"

# to debug multiple:
set -x DEBUG "update build parse"
```


If executing the node script from a bash shell:

```bash
# to debug everything:
export DEBUG="*"

# to debug specific:
export DEBUG="build"

# to debug multiple:
export DEBUG="update build parse"
```




## Environmental variables.
#! Auditing 'dotenv' --likely unneccessary

The most sure-fire way of applying environmental variables to node scripts is to use the `dotenv` package. Require 'dotenv' and run its `config` function.

However, the default function is to run the `.env` file within the same directory as the `package.json` file. To run Webkit as a CLI tool, must be able to supply `Webkit.env` file.

