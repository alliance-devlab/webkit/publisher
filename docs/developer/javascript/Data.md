### [Immutable.js](https://immutable-js.com/)
Use Immutable to store password on Webkit Service.


### (NeJS](https://github.com/louischatriot/nedb)

In-memory or file-based.


#### Async Queries with NeDB:

```js
const Store = require('nedb')
let db = new Store()

const query = (q={}) => new Promise((resolve, reject) => {
   db.findOne(q, (err, publisher) => {
      if (err) reject(err)
      else resolve(publisher)
   })
})

async function test () {
   let doc = await query()
   console.dir(doc)
}
```


### SQLite + [Sequalize](https://www.npmjs.com/package/sequelize)
