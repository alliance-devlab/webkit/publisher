## Webkit Sentinel, remote content builder.
Sentinel is essentially content building process "in the cloud" that awaits new content submissions and builds them for publication.

There are many potential strategies:
 * _Dedicated server_: Node.js API
   - Capability: P2P Signalling Server 
   - Federated Search?

 * Gitlab Continuous Integration
   - Building capabilities only.

 * Kubernetes/Fission.io
   - More robust scripting than CI?
   - Building capabilities only?


### Option #3: Kubernetes/Fission.io

[Linode Kubernetes Engine (LKE)](https://www.linode.com/docs/guides/deploy-and-manage-a-cluster-with-linode-kubernetes-engine-a-tutorial/
)

[Setup Kubernetes, Official Guide](https://kubernetes.io/docs/setup/)



### Use Fission.io to host "serverless" functions:
[Fission.io](https://fission.io/docs/installation/)
```sh
# Add the stock NodeJS env to your Fission deployment
fission env create --name nodejs --image fission/node-env

# A javascript function that prints "hello world"
curl -LO https://raw.githubusercontent.com/fission/examples/main/nodejs/hello.js

# Upload your function code to fission
fission function create --name hello-js --env nodejs --code hello.js

# Test your function.  This takes about 100msec the first time.
fission function test --name hello-js
Hello, world!
```