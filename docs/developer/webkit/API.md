### Express.js



### [Feathers.js](https://feathersjs.com/)






### Webkit Login Service
The Webkit Publisher login service is a simple Rest API ([Restify](https://www.npmjs.com/package/restify)) and an in-memory database ([NeDb](https://www.npmjs.com/package/nedb)).

The login service uses [Http Signature](https://www.npmjs.com/package/http-signature) to authorize HTTP access via `localhost:9007`

The API is initialized at the `/login` endpoint with:

```js
let daemon = new WebkitDaemon(publisher);


//?
let { pw, nid } = publisher   //logged in account
//nid = 'steward@webkit.ipfs'
//pw  = 'secret'
let key = sh.execSync(`gpg --pinentry-mode loopback --passphrase=${pw} --armor --export-secret-key ${nid}`).toString().trim()
```

### Generate `session.pgp` token (for HTTP Signature to secure API):
```js
module.exports = async function (publisher) {
   let { id, nid } = publisher
   let token = Path.join(HOME, `.config/webkit/publishers/${id}/.keys/session.pgp`)

   const { privateKey } = await OpenPGP.generateKey({
      type: 'ecc', // Type of the key, defaults to ECC
      curve: 'curve25519', // ECC curve name, defaults to curve25519
      userIDs: [{ name: publisher.id, email: publisher.nid }],
      //?passphrase: publisher.pw, // protects the private key
      format: 'armored' // output key format, defaults to 'armored' (other options: 'binary' or 'object')
  });

  fs.writeFileSync(token, privateKey)
}

```


 * Generate a "Login Token"
 * gen ssh ${nid}
