## Webkit CLI

The Webkit CLI must be available to any directory on the machine, so the `webkit` executable must be available from the shell.

Let's try that. Create a symlink to `Webkit/bin/webkit` in `~/.local/bin`

## Webkit Config
Webkit config is stored at `$HOME/.config/webkit`

Within the `.config/webkit` directory are `publisher` and `sites` subdirectories:

```
webkit
├── publisher
└── sites
```

The `publisher` subdirectory stores the webkit publisher credentials (webmaster). The `sites` subdirectory stores site-specific credentials and data.


## Render Markdown on the command-line:
```
sudo snap install glow
echo " # This is markdown." | glow -
```

## Generate man pages from markdown:
https://rtomayko.github.io/ronn/ronn.1.html



[oclif.io, "The Open CLI Framework"](https://oclif.io/)

[GlueGun, "Typescript-powered command-line apps"](https://github.com/infinitered/gluegun#readme)

## Enquirer (alt to Inquirer):
https://github.com/enquirer/enquirer