

### [Claiming DNS on Handshake](https://github.com/handshake-org/hsd#claiming-a-name)

If you own a name in the existing root zone or the Alexa top 100k, your name is waiting for you on the blockchain. You are able to claim it by publishing a DNSSEC ownership proof -- a cryptographic proof that you own the name on ICANN's system.

Your name must have a valid DNSSEC setup in order for the claim to be created. If you do not have DNSSEC set up, don't worry -- you can set it up after the handshake blockchain launches and proofs will still be accepted retroactively. Here's some useful guides for setting DNSSEC up on popular DNS services:

Namecheap: https://www.namecheap.com/support/knowledgebase/subcategory.aspx/2232/dnssec
GoDaddy: https://www.godaddy.com/help/dnssec-faq-6135
Gandi: https://wiki.gandi.net/en/domains/dnssec
Name.com: https://www.name.com/support/articles/205439058-Managing-DNSSEC
Hover: https://help.hover.com/hc/en-us/articles/217281647-Understanding-and-managing-DNSSEC
Cloudflare: https://support.cloudflare.com/hc/en-us/articles/209114378
If you run your own nameserver, you're going to need some tools for managing keys and signing your zonefile. BIND has a number of command-line tools for accomplishing this:

https://linux.die.net/man/8/dnssec-keygen
https://linux.die.net/man/8/dnssec-dsfromkey
https://linux.die.net/man/8/dnssec-signzone


### DNSSEC (Domain Name Security Extensions)
https://www.godaddy.com/help/what-is-dnssec-6135

Domain Name Security Extensions (DNSSEC) is an advanced DNS feature that adds an extra layer of security to your domains by attaching digital signature (DS) records to their DNS information. Upgrade to Premium DNS and you can enable DNSSEC in your account. If you're using self-managed DNSSEC, you can manually add a DS record in your account.

