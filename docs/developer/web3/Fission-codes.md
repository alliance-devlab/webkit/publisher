## [Fission.codes CLI](https://guide.fission.codes/developers/cli)

```fish
fission setup -u $username -e $email
```

`fission setup --help`:
Usage: fission setup [--os OS] [-u|--username USERNAME] [-e|--email EMAIL] 
                     [-k|--with-key KEYFILE]
  Initial Fission setup

Available options:
  --os OS                  Override OS detection
  -u,--username USERNAME   The username to register
  -e,--email EMAIL         The email address for the account
  -k,--with-key KEYFILE    A root keyfile to import
  -h,--help                Show this help text



### Setup with an existing key
You can set up your machine with an existing account if you have the machine_id.ed25519 key. Use --with-key argument and the path to your key.
```sh
fission setup --with-key ~/path/to/machine_id.ed25519 
```







[Webnative SDK package](https://www.npmjs.com/package/webnative)

```js
import * as wn from 'webnative'

const state = await wn.initialise({
  permissions: {
    // Will ask the user permission to store
    // your apps data in `private/Apps/Nullsoft/Winamp`
    app: {
      name: "Winamp",
      creator: "WebkitPublisher"
    },

    // Ask the user permission to additional filesystem paths
    fs: {
      private: [ wn.path.directory("Audio", "Music") ],
      public: [ wn.path.directory("Audio", "Mixtapes") ]
    }
  }

}).catch(err => {
  switch (err) {
    case wn.InitialisationError.InsecureContext:
      // We need a secure context to do cryptography
      // Usually this means we need HTTPS or localhost

    case wn.InitialisationError.UnsupportedBrowser:
      // Browser not supported.
      // Example: Firefox private mode can't use indexedDB.
  }

})


switch (state.scenario) {

  case wn.Scenario.AuthCancelled:
    // User was redirected to lobby,
    // but cancelled the authorisation
    break;

  case wn.Scenario.AuthSucceeded:
  case wn.Scenario.Continuation:
    // State:
    // state.authenticated    -  Will always be `true` in these scenarios
    // state.newUser          -  If the user is new to Fission
    // state.throughLobby     -  If the user authenticated through the lobby, or just came back.
    // state.username         -  The user's username.
    //
    // ☞ We can now interact with our file system (more on that later)
    state.fs
    break;

  case wn.Scenario.NotAuthorised:
    wn.redirectToLobby(state.permissions)
    break;

}

```



[Webnative SDK docs](https://guide.fission.codes/developers/webnative)

