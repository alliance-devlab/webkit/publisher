# [InterPlanetary Linked Data](https://ipld.io/docs/)
IPLD stands for InterPlanetary Linked Data.

IPLD is an ecosystem of formats and data structures for building applications that can be fully decentralized.

The goal of IPLD is to enable decentralized data structures that are universally addressable and linkable, which in turn will enable more decentralized applications. These addressable and linkable data structures will allow us to do for data what URLs and links did for HTML web pages.



## [IPLD Explorer](https://explore.ipld.io/)






## Javascript Implimentation:

### [js-multiformats](https://github.com/multiformats/js-multiformats) impliments IPLD in javascript.

#### Interfaces:
```js
import { CID } from 'multiformats/cid'
import * as json from 'multiformats/codecs/json'
import { sha256 } from 'multiformats/hashes/sha2'

const bytes = json.encode({ hello: 'world' })

const hash = await sha256.digest(bytes)
const cid = CID.create(1, json.code, hash)
//> CID(bagaaierasords4njcts6vs7qvdjfcvgnume4hqohf65zsfguprqphs3icwea)
```

#### Creating Blocks:
```js
import * as Block from 'multiformats/block'
import * as codec from '@ipld/dag-cbor'
import { sha256 as hasher } from 'multiformats/hashes/sha2'

const value = { hello: 'world' }

// encode a block
let block = await Block.encode({ value, codec, hasher })

block.value // { hello: 'world' }
block.bytes // Uint8Array
block.cid   // CID() w/ sha2-256 hash address and dag-cbor codec

// you can also decode blocks from their binary state
block = await Block.decode({ bytes: block.bytes, codec, hasher })

// if you have the cid you can also verify the hash on decode
block = await Block.create({ bytes: block.bytes, cid: block.cid, codec, hasher })
```

#### Multibase Encoders / Decoders/ Codecs:
```js
import { base64 } from "multiformats/bases/base64"
cid.toString(base64.encoder)
//> 'mAYAEEiCTojlxqRTl6svwqNJRVM2jCcPBxy+7mRTUfGDzy2gViA'
```

#### Traversal:
```js
import { walk } from 'multiformats/traversal'
import * as Block from 'multiformats/block'
import * as codec from 'multiformats/codecs/json'
import { sha256 as hasher } from 'multiformats/hashes/sha2'

// build a DAG (a single block for this simple example)
const value = { hello: 'world' }
const block = await Block.encode({ value, codec, hasher })
const { cid } = block
console.log(cid)
//> CID(bagaaierasords4njcts6vs7qvdjfcvgnume4hqohf65zsfguprqphs3icwea)

// create a loader function that also collects CIDs of blocks in
// their traversal order
const load = (cid, blocks) => async (cid) => {
  // fetch a block using its cid
  // e.g.: const block = await fetchBlockByCID(cid)
  blocks.push(cid)
  return block
}

// collect blocks in this DAG starting from the root `cid`
const blocks = []
await walk({ cid, load: load(cid, blocks) })

console.log(blocks)
//> [CID(bagaaierasords4njcts6vs7qvdjfcvgnume4hqohf65zsfguprqphs3icwea)]
```



### Data Model and Codecs:
Firstly, we define a "Data Model" which says what the domain and range of data is. This is important because its the foundation of everything else we will build.

(Broadly, we can say the Data Model is "like JSON", and you've probably got the right idea -- maps, strings, lists, etc.)

Thereafter, we define "Codecs", which say how it can be parsed from serial messages and emitted as serial messages.

IPLD has lots of Codecs. You can choose to use different codecs based on what other applications you'd like to interface with, or just based on what fits the performance vs human-readability that you prefer for your own applications.

### Linking:
A key part of IPLD is its ability to link together documents.

IPLD linking isn't like some other forms of linking, like URLs (which refer to "locations" of data) -- instead, it's based on content-addressing (which means refering to data by a hash of its content). IPLD uses a format called CIDs for this, to be specific.

What's neat about content-addressing is that because it does not involve talking about data location, it's inherently friendly to decentralization. This means large graphs of documents can be linked together, and once you have some of the document graph, you don't need to go look online to some specific server to get the rest of the linked documents; you can get them anywhere that content can be found.

Content-addressing also separates document identity from discussion of authority. This again contributes to decentralization-friendly systems: once you get part of a document graph, and have decided it's the document you're looking for, you can get all the related documents without having to bounce through some other system to re-determine what the authoratitive document is for every related document -- you already know what that is, because you already have its content ID.

### Powerful Features: Schemas
Then we provide a couple other ways to handle data via the Data Model: **Schemas**, which can _describe the structure of data_, and be used for validation, detecting structure, and some kinds of basic data transformation; and **Advanced Data Layouts**, which let us do things like assemble complex data structures to be presented as simpler ones (so you can work on them "like basic Data Model", even if they have more power, such as sharded, or encryption, or etc).

  - The `Universal Interface` is the interface that applications use which _abstracts over top of the raw data model, Schemas and ADLs_.It can then be used by higher level APIs to load and manipulate data.

 - The `Raw Data Model` is _how IPLD represents data_. 
   - Designed for maximal utility

 - `Schemas` are a means of _formalizing the shape of data_ structures within the bounds of the `Data Model` and may present the data in an altered form. (e.g. a "struct" containing a fixed list of fields, serialized as an array with fixed ordering).

 - `Advanced Data Layouts` couple _programmatic functionality_ with data structures, allowing for complex data structuring while presenting as the Data Model (e.g. encryption, large multi-block spanning data structures). Note that ADL APIs are more stable for reading than writing as write APIs are under development.
 
 - `Codecs` are bound by the data model to include a set of data types, including links (currently represented as CIDs). Codecs supporting a subset of the Data Model (DAG-PB, plain CBOR, plain JSON, etc.) likewise have limited support from functionality from Data Model components.

 - `Blocks` are arbitrary _arrays of bytes identified by a CID_.   
   - IPLD doesn't concern itself with the source or nature of these bytes as long as its Codecs can read and/or write them.
   - Limitations (size, location, availability, etc.) are concerns of the data source.

 - You can `Print` IPLD data in a human-readable format which preserves it's structure.

 - A `Path` is a simple textual description of how to move from one node in the Data Model to another node that is a child (or grandchild, etc).
   - A key benefit of having a standardized Data Model is that we can define Pathing over it
 - A Path is composed of `PathSegments`.
   - Each PathSegment is either _a **key** for traversing across a map_, or _an **index** for traversing across a list_.
   - A Path defines a singular navigation through a DAG.
   - Visting multiple nodes in a graph requires Selectors.

 - The combination of a `CID` and a `Path` is a context-free way to reference any information that's legible via IPLD. (This is sometimes called a "merklepath".)


 - `Selectors` are _expressions that describe a traversal_ over an IPLD dag and "select" a subset of nodes during that walk. Selectors are a **declarative format** for _specifying a DAG traversal_, both stating what parts of the graph to traverse, and providing _a way to mark certain nodes as highlighted_.
 
   - Most IPLD libraries will provide a native function for evaluating Selectors.
      (typically this involves callbacks for visiting the "highlighted" nodes).

 - `Traversal` is the act of _following links_ and fields inside IPLD data.

  - You can `Transform` IPLD data using different techniques like traversing it and using selectors to choose nodes to modify.

 - You can also `Patch` data using the Universal Interface to apply changes to it. Note that patch APIs are currently under development.

 - `Graphsync` is a protocol which builds on Selectors, and aims to allow two or more communicating agents _to exchange Blocks of data efficiently_: by **describing what parts of the graph they're interested in exchanging**, the number of round-trips required to communicate groups of related Blocks can be greatly reduced.

 - The `Data Model Node` interface carries a lot more weight that ust that of the `Data Model` itself —it also defines the necessary (and in many ways the possible) behavior of Schema systems and ADLs as well; Codecs are defined entirely in terms of it; and it is the center of all Pathing and other forms of traversal (such as Selectors) as well.



### [IPLD Schemas](https://ipld.io/docs/schemas/):
IPLD Schemas are an [interface definition language (IDL)](https://en.wikipedia.org/wiki/Interface_description_language). They provide a way for protocol authors to define types, and process data using those types. They can be used to give structure to protocol development, to save developer effort by providing common patterns of data arrangement and data validation, and enable other powerful development tooling.

IPLD Schemas are meant to be useful as a _design language_: they should make specifying protocols and coordinating groups of developers and their applications easier.

IPLD Schemas are founded on the principle of _structural typing_. **Structural typing** means that the names of types aren't expected to be found in the serialized data; instead, whether a schema matches the data or not is decided based whether the structural outline of the data matches the structural description in the schema. The practical impact of this is IPLD Schemas can be used _to describe pre-existing data_.

IPLD Schemas are also noteworthy for the defintion of a _type_ (the logical view of the data) also requiring _an associated representation strategy_ for that type (which says how it is mapped into the IPLD Data Model, and thence **how it's serialized**).

#### The Schema-Schema
 * [Annotated](https://ipld.io/specs/schemas/schema-schema.ipldsch)
 * [Raw JSON](https://ipld.io/specs/schemas/schema-schema.ipldsch.json)

The schema-schema is intentionally designed to be relatively easy to parse. We chose to use features when defining the schema-schema that are the clearest to implement, and the most likely to be early selections for a high priority in implementing anyway; and we in some cases carefully avoided using features that would've been nice-to-have but technically nonessential.


#### Strings versus Bytes:
Strings are really just bytes: they can contain the same range of data (anything). However, strings carry _the implication that they are **human readable**_.

 - Strings tend to be treated differently by various presentation layers and user-facing tools and debugging representations.
 - Strings and bytes are also serialized distinctly in many Codecs.


#### Maps:
Maps have a stable, defined iteration order. But it's not necessarily a `sorted order`.

This is necessarily the case, because some ADLs (in fact, HAMTs, specifically) have a natural iteration order, and _implementing any other iteration order would be prohibitively expensive_, and yet that order is not a sorted order.

Some Codecs specify a `sorted order`. If they do so, note that that's the codec's choice; the Data Model is more expressive, since it can persist any order.

 * Map keys are strings (not bytes)


#### IPLD Schemas [ipld.io/docs](https://ipld.io/docs/intro/primer/):
 * `Schema` provides developer-friendly ways to describe outlines of data structures that a program wants to handle.
   - **Schemas** can be used _to describe anything in IPLD_, even if it was made without use of Schemas.
   - Schemas describe two things: **type** information, which is all about semantics, and **representation information**, which is all about how the information is represented at the Data Model level.
   - Schemas support a superset of the same "types", supported as "kinds" in the Data Model (eg, `map`, `list`, `string`) as well as a few more (eg, `struct`, `union` ("sum types"), `enum`)
   - Schemas support _a variety of possible representations_ for each type.

   - Every Schema type also has a defined way of being percieved as Data Model, which means that you can always _apply Pathing over the parsed Schema data_ just like you would the raw data.

   - Prefer *not* to embed a reference to an IPLS `Schema` directly within the document, because:
      - `explicit versioning` is fragile; whereas `feature detection` allows smooth growth and natural evolution.

#### IPLD Advanced Data Layouts (ADL):
ADLs support topological transformations of data. 

An ADL is distinct from a `Codec`, in that the raw input is also `Data Model` (though it may be one `Node` or many, and the input may even span multiple Blocks. The purpose of an `ADL` is _to make data legible as a Data Model Node_.


 * Having _large maps_ is important. 
  * Examples:
   - `directories` in _filesystem-like applications_. 

 * When we want **maps that might be larger than can fit reasonably well into a single Block**, we recommend using the ADL called HAMT:
 
  * `HAMT` _splits the `map` data across many blocks_, somewhat like a B+ tree, but presents the whole thing **as if it's a single map** and provides a programming interface, conforming to the Data Model, Node interfaces.

 * Having _large byte sequences_ is important. 
 * Examples:
   - Storing `files`!
   
 * For **`byte sequences` that might be larger than can fit well into a single `Block`**, we recommend using the ADL called FBL:
  * `FBL` _splits the `byte sequence` across many blocks_, somewhat like a B+ tree, but presents the whole thing **as if it's a single contiguous byte sequence**, and provides a programming interface, conforming to the Data Model, Node interfaces.

Encrypting data is neat. We suspect [[ADL]]s are suitable for describing this, and making encryption operate smoothly within IPLD, without baking any particular algorithm or constructions into IPLD (which would make them harder to innovate on and change). We don't have any fully worked examples of this yet, however.




[IPLD Github repo](https://github.com/ipld/)

 * [ipld](https://github.com/ipld/ipld)
 * [js-ipld](https://github.com/ipld/js-ipld)


IPLD uses CBOR for data serialization, and have a deep interest in canonicalization and applications of hashing.

