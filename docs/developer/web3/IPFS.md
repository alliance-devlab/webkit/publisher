---
media:
  turtles: https://ipfs.io/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco/wiki/Turtles_all_the_way_down.html
---


## Content Identifiers (CIDs):
Because IPFS uses content addressing rather than the legacy web's method of location addressing, each piece of data stored in the IPFS network gets its own unique content identifier (CID). Copies of the data associated with that CID can be stored in any number of locations worldwide on any number of participating IPFS nodes. To make retrieving the data associated with a particular CID efficient and robust, IPFS uses a distributed hash table (DHT) to keep track of what's stored where.

When you use IPFS to retrieve a particular CID, your node queries the DHT to find the closest nodes to you with that item — and by default also agrees to re-provide that CID to other nodes for a limited time until periodic "garbage collection" clears your cache of content you haven't used in a while.

### Usage:[ipfs refs](https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/REFS.md)

Get links(references) from an object:
```fish
ipfs refs Qmb9q2BA4rHnUUWh2RHSQFRWHJ8pxjJ7ZUevQL7eqP5Ztv
# QmVjtmiSTuZq6oa8b4NAkqeY1xpuNJS8zcWWhnUmTh52pH
# QmbF5kBFcqPgvxJvUEL9oqkioGUxAyCAmdLxL9MGq4aEem
```

Output all local references (CIDs of all blocks in the `blockstore`)
```fish
ipfs refs local
# bafkreiavtjtdganb7qgusqmxpijzxa5zgq7s3hvhjzhsyh5umfpvc6vlmm
# bafkreihjf3t3tap7uh62wolu42h5ztkpeh4kgmzarflhtnswiludzdacgi
# ....
```
Blocks in the blockstore are stored by multihash and not CID so yielded CIDs are v1 CIDs with the 'raw' codec. These may not match the CID originally used to store a given block, though the multihash contained within the CID will.


## Controlling what you share:
By default, an IPFS node announces to the rest of the network that it is willing to share every CID in its cache (in other words, _reproviding content_ that it's retrieved from other nodes), as well as CIDs that you've explicitly pinned or added to MFS to make them consistently available. If you'd like to disable this behavior, you can do so in the reprovider settings (opens new window)of your node's config file.


## [IPFS Pinning](https://docs.ipfs.io/how-to/pin-files/)
`Pinning` is the mechanism that allows you to tell IPFS to always keep a given object somewhere —the default being your `local node`, though this can be different if you use a third-party `remote pinning service`.

You can also "pin" CIDs that you want to make sure are never garbage-collected —either explicitly using IPFS's low-level pin API or implicitly using the `Mutable File System ` (IPFS `files`) — which also means you're acting as a permanent _reprovider_ of that data.

It's important to note that those `DHT queries` _happen in public_. Because of this, it's possible that third parties could be monitoring this traffic to determine what CIDs are being requested, when, and by whom. As IPFS continues to grow in popularity, it's more likely that such monitoring will exist.

Three kinds of pins:
 1. Direct Pins, a single IPLD node.
 2. Recursive Pins, node and all its descendents.
 3. Indirect Pins, descendent of a directly pinned node.

 * A pinned object cannot be garbage-collected.


## Garbage Collection:
`ipfs repo gc`



## Encryption, [Noise Protocol](https://noiseprotocol.org/noise.html)
Noise is a framework for crypto protocols based on Diffie-Hellman key agreement. Noise can describe protocols that consist of a single message as well as interactive protocols.

A Noise protocol begins with two parties exchanging `handshake messages`. During this `handshake phase` the parties exchange DH public keys and perform a sequence of DH operations, hashing the DH results into a shared secret key. After the handshake phase each party can use this shared key to send encrypted `transport messages`.

The Noise framework supports handshakes where each party has a long-term `static key pair` and/or an `ephemeral key pair`. A Noise handshake is described by a simple language. This language consists of `tokens` which are arranged into `message patterns`. Message patterns are arranged into `handshake patterns`.

A `message pattern` is a sequence of tokens that specifies the DH public keys that comprise a handshake message, and the DH operations that are performed when sending or receiving that message. A `handshake pattern` specifies the sequential exchange of messages that comprise a handshake.

A handshake pattern can be instantiated by `DH functions`, `cipher functions`, and `hash functions` to give a concrete _Noise protocol_.

While today's encryption might seem bulletproof right now, there is no guarantee that it won't be broken at some point in the future. Future breakthroughs in computing might allow going back and decrypting older content that's been put on a public network, such as IPFS. 

If you want to guard against this potential attack vector, using IPFS `hybrid-private networks` — in which nodes sit behind connection gates that check request ACLs before giving a node a request — is a potential design direction.


## Private IPFS Networks:
Allows IPFS node only to connect to other peers who have a shared secret key.

 * To be extra cautious, you can also set the `LIBP2P_FORCE_PNET` environment variable to `1` to _force the usage of private networks_. 

 * If no private network is configured, the daemon will fail to start.

Generate a pre-shared-key using [ipfs-swarm-key-gen](https://github.com/Kubuxu/go-ipfs-swarm-key-gen):

  ```fish
  go get github.com/Kubuxu/go-ipfs-swarm-key-gen/ipfs-swarm-key-gen
  ipfs-swarm-key-gen > ~/.ipfs/swarm.key
  ```
  Looks like a simple, random 32-byte array:
  ```go
  package main

  import "crypto/rand"
  import "encoding/hex"
  import "fmt"
  import "log"

  func main() {
    key := make([]byte, 32)
    _, err := rand.Read(key)
    if err != nil {
      log.Fatalln("While trying to read random source:", err)
    }

    fmt.Println("/key/swarm/psk/1.0.0/")
    fmt.Println("/base16/")
    fmt.Print(hex.EncodeToString(key))
  }
  ```

**To join a given _private network_**, get the `swarm key` file from someone in the network and save it to `~/.ipfs/swarm.key` (If you are using a custom $IPFS_PATH, put it in there instead).

When using this feature, you will not be able to connect to the default bootstrap nodes (Since we aren't part of your private network) so you will need to set up your own bootstrap nodes.

 * To prevent your node from even trying to connect to the default bootstrap nodes, run:
  - `ipfs bootstrap rm --all`
 * Then add your own bootstrap peers with:
  - `ipfs bootstrap add <multiaddr>`
  - eg, `ipfs bootstrap add /ip4/104.236.76.40/tcp/4001/p2p/QmSoLV4Bbm51jM9C4gDYZQ9Cy3U6aXMJDAbzgu2fzaDs64`

**Bootstrap nodes** are no different from all other nodes in the network apart from the function they serve.


### [IPFS Cluster]()

## Kubernetes using [Kustomize.io](https://kustomize.io/) / [Resource Config](https://kubectl.docs.kubernetes.io/guides/config_management/introduction/)
https://cluster.ipfs.io/documentation/guides/k8s/

## [TrueNAS](https://www.truenas.com/docs/solutions/integrations/containers/)

[TrueNAS + Docker/Kubernetes Storage](https://www.truenas.com/blog/truenas-enables-container-storage-and-kubernetes/)

[democratic-csi](https://github.com/democratic-csi/democratic-csi)





## [Repo](https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/REPO.md)
```fish
ipfs repo stat
# NumObjects: 1264
# RepoSize:   30536799
# StorageMax: 10000000000
# RepoPath:   /home/yarrow/.ipfs
# Version:    fs-repo@12
```

## [Stats]()

 * bandwidth: `ipfs stats bw`:
 ```sh
 # Bandwidth
 # TotalIn: 185 MB
 # TotalOut: 59 MB
 # RateIn: 37 kB/s
 # RateOut: 952 B/s
 ```

 * bitswap `ipfs stats bitswap`:
 ```sh
# bitswap status
#   provides buffer: 0 / 256
#   blocks received: 1134
#   blocks sent: 11
#   data received: 1896187
#   data sent: 644707
#   dup blocks received: 90
#   dup data received: 91264
#   wantlist [0 keys]
#   partners [566]
 ```

 * provider system `ipfs stats provide`:
 ```sh
 # Error: can only return stats if
 # `Experimental.AcceleratedDHTClient` is enabled
 ```


## [CIDs are not File Hashes (sha256)](https://docs.ipfs.io/concepts/hashing/#content-identifiers-are-not-file-hashes)
You may be used to verifying the integrity of a file by matching SHA hashes, but SHA hashes won't match CIDs. Because IPFS splits a file into blocks, each block has its own CID, including separate CIDs for any parent nodes.



## MerkelDag:
https://dag.ipfs.io/


The DAG keeps track of all the content stored in IPFS as blocks, not files, and Merkle DAGs are self-verified structures. To learn more about DAGs, see directed acyclic graph (DAG).


## Multiformats







## Distributed Hash Tables (DHTs):
To find which peers are hosting the content you're after (discovery), IPFS uses a distributed hash table, or DHT. A `hash table` is a _database of keys to values_. A `distributed hash table` is one where _the table is split across all the peers in a distributed network_. To find content, you ask these peers.

The `libp2p` project is the part of the IPFS ecosystem that provides the **DHT** and _handles peers connecting and talking to each other_.

Once you know where your content is (or, more precisely, which peers are storing each of the blocks that make up the content you're after), you use the DHT again to find the current location of those peers (routing). So, to get to the content, use libp2p to query the DHT twice.

## LibP2P:
What makes libp2p especially useful for _peer-to-peer connections_ is `connection multiplexing`. Traditionally, every service in a system opens a different connection to communicate with other services of the same kind remotely. Using IPFS, you open just one connection, and you multiplex everything on that. For everything your peers need to talk to each other about, you send a little bit of each thing, and the other end knows how to sort those chunks where they belong.

This is useful because establishing connections is usually hard to set up and expensive to maintain. With **multiplexing**, once you have that connection, you can do whatever you need on it.

## Bitswap:
[Bitswap](https://github.com/ipfs/specs/blob/master/BITSWAP.md) allows you to connect to the peer or peers that have the content you want, send them your wantlist (a list of all the blocks you're interested in), and have them send you the blocks you requested. Once those blocks arrive, you can verify them by hashing their content to get CIDs and compare them to the CIDs that you requested. These CIDs also allow you to deduplicate blocks if needed.


## DNS Links:
[resolve links](https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/MISCELLANEOUS.md#ipfsdnsdomain-options)
```fish
ipfs dns _dnslink.ipfs.io
```

## Integrated Pinning:
[pinning](https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/PIN.md#ipfspinremoteserviceaddname-options)
```js
await ipfs.pin.remote.service.add('pinata', {
  endpoint: new URL('https://api.pinata.cloud'),
  key: 'your-pinata-key'
})
```

## [PubSub](https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/PUBSUB.md)




## Graphsync:
There are other content replication protocols under discussion as well, the most developed of which is [Graphsync](https://github.com/ipld/ipld/blob/c8ea2101248f0fcde9f2b069f1eac41e0fced870/specs/transport/graphsync/index.md).

**Graphsync** is a protocol to _synchronize graphs across peers_ that uses `IPLD Selectors` to effeciently transfer graphs (or selections of parts of graphs) with a minimal number of independent requests, and thus seeks to attain low overhead for high latency situations.

 * Possible use cases:
  - Downloading Package Dependencies:
    * Give me everything within <hash>/foo/v1.0.0
  - Loading content from deep within a giant dataset:
    * Give me the nodes for the path <hash>/a/b/c/d/e/f/g
  - Loading a large video optimizing for playback and seek
    * First, give me the first few data blocks <hash>/data/*
    * Second, give me all of the tree except for leaves <hash>/**/!
    * Third, give me everything else. <hash>/**/*

### Javascript implimentation:
[@dcdn/graphsync](https://www.npmjs.com/package/@dcdn/graphsync)

Stream files directly between peers using LibP2P + GraphSync

```js
import {create, getPeer} from "libp2p";
import {Noise} from "@chainsafe/libp2p-noise";
import WebSockets from "libp2p-websockets";
import Mplex from "libp2p-mplex";
import {MemoryBlockstore} from "blockstore-core/memory";
import {GraphSync, unixfsPathSelector} from "@dcdn/graphsync";

const blocks = new MemoryBlockstore();

const libp2p = await createLibp2p({
  modules: {
    transport: [WebSockets],
    connEncryption: [new Noise()],
    streamMuxer: [Mplex],
  },
  config: {
    peerDiscovery: {
      autoDial: false,
    },
  },
});
await libp2p.start();
    
const exchange = new GraphSync(libp2p, blocks);

const provider = getPeer("/ip4/127.0.0.1/tcp/41505/ws/p2p/12D3KooWCYiNWNDoprcW74NVCEKaMhSbrfMvY4JEMfWrV1JamSsA");
libp2p.peerStore.addressBook.add(provider.id, provider.multiaddrs);
const [cid, selector] = unixfsPathSelector("bafyreiakhbtbs4tducqx5tcw36kdwodl6fdg43wnqaxmm64acckxhakeua/Cat.jpg");

const request = exchange.request(cid, selector);
request.open(provider.id);
// Save the blocks into the store;
await request.drain();
```




## The JavaScript implementation of the IPFS protocol
[js-ipfs](https://github.com/ipfs/js-ipfs/tree/master#packages)

[js-ipfs examples](https://github.com/ipfs-examples/js-ipfs-examples/tree/master/examples)
   * [traverse IPLD graphs](https://github.com/ipfs-examples/js-ipfs-examples/tree/master/examples/traverse-ipld-graphs)

   * [video streaming (HLS)](https://github.com/ipfs-examples/js-ipfs-examples/tree/master/examples/browser-video-streaming)

   * [exchange files between browsers (websockets)](https://github.com/ipfs-examples/js-ipfs-examples/blob/master/examples/browser-exchange-files/src/app.js)
      - using public webrtc-star server:
      '/dns4/wrtc-star1.par.dwebops.pub/tcp/443/wss/p2p-webrtc-star'



[ipfs packages](https://github.com/ipfs/js-ipfs/tree/master/packages)
 * ipfs
 * ipfs-core
 * ipfs-cli
 * interface-ipfs-core
 * ipfs-cli
 * ipfs-client
 * ipfs-core-config
 * ipfs-core-types
 * ipfs-core-utils
 * ipfs-core
 * ipfs-daemon
 * ipfs-grpc-client
 * ipfs-grpc-protocol
 * ipfs-grpc-server
 * ipfs-http-client
 * ipfs-http-gateway
 * ipfs-http-response
 * ipfs-http-server
 * ipfs-message-port-client
 * ipfs-message-port-protocol
 * ipfs-message-port-server



# IPFS

 * Get PeerID & PublicKey:
 ```sh
  ipfs id | jq
 ```

 * Get PrivateKey:
```sh
  cat ~/.ipfs/config | jq .Identity
```


### [Merkle Directed Acyclic Graphs (DAGs)](https://docs.ipfs.io/concepts/merkle-dag)
IPFS and many other distributed systems take advantage of a data structure called **directed acyclic graphs**, or **DAGs**. Specifically, they use **Merkle DAGs**, where _each node has a unique identifier that is a hash of the node's contents_.

A **Merkle DAG** is a DAG where each node has an identifier, and this is the result of hashing the node's contents — any opaque payload carried by the node and the list of identifiers of its children — using a cryptographic hash function like SHA256. This brings some important considerations:

 * **content addressing**: Identifying a data object (like a Merkle DAG node) by the value of its hash.
 * Each **node identifier** is as Content Identifier, or **CID**.
 * Merkle DAGs can only be constructed from the **leaves**, that is, from _nodes without children_. 
 * Parents are added after children because the children's identifiers must be computed in advance to be able to link them.
 * Every node in a Merkle DAG is the root of a (sub)Merkle DAG itself, and this subgraph is contained in the parent DAG.
 * Merkle DAG nodes are **immutable**.
    - Any change in a node would alter its identifier and thus affect all the ascendants in the DAG, _essentially creating a different DAG_.
 * Several branches can **re-converge** or, in other words, _a node can have several parents_.
 * Merkle DAGs are **self-verified structures**.
    - The CID of a node is univocally linked to the contents of its payload and those of all its descendants.
    - Thus two nodes with the same CID univocally represent exactly the same DAG.
    - This will be a key property to efficiently sync Merkle-CRDTs (**Conflict-free Replicated Data Types**).
    - Source control systems like git and others use them to efficiently store the **repository history** in a way that enables _de-duplicating the objects and detecting conflicts between branches_.

 * [DAG Builder visualizer](https://dag.ipfs.io/)

To build a Merkle DAG representation of your content, IPFS often first splits it into **blocks**. Splitting it into blocks means that different parts of the file can come from different sources and be authenticated quickly. (If you've ever used BitTorrent, you may have noticed that when you download a file, BitTorrent _can fetch it from multiple peers at once_; this is the same idea.)

Merkle DAGs are a bit of a ["turtles all the way down"](turtles) scenario; that is, everything has a CID. Let's say you have a file, and its CID identifies it. What if that file is in a folder with several other files? Those files will have CIDs too. What about that folder's CID? It would be a hash of the CIDs from the files underneath (i.e., the folder's content). In turn, those files are made up of blocks, and each of those blocks has a CID. You can see how a file system on your computer could be represented as a DAG. You can also see, hopefully, how Merkle DAG graphs start to form. For a visual exploration of this concept, take a look at the [IPLD Explorer](https://explore.ipld.io/#/explore/QmSnuWmxptJZdLJpKRarxBMS2Ju2oANVrgbr2xWbie9b2D)

Another useful feature of Merkle DAGs and breaking content into blocks is that if you have two similar files, they can share parts of the Merkle DAG, i.e., parts of **different Merkle DAGs can reference the same subset of data**. For example, if you update a website, only updated files receive new content addresses. Your old version and your new version can refer to the same blocks for everything else.

### Use `ipfs-unixfs` for uploading a directory of files:
[javascript implimentation](https://www.npmjs.com/package/ipfs-unixfs)
JavaScript implementation of IPFS' UnixFS (a Unix FileSystem files representation on top of a MerkleDAG)

 * [js-ipfs-unixfs ipfs-unixfs-importer](https://github.com/ipfs/js-ipfs-unixfs/tree/master/packages/ipfs-unixfs-importer)
  **JavaScript implementation of the layout and chunking mechanisms used by IPFS to handle Files**

 * [js-ipfs-unixfs-exporter](https://github.com/ipfs/js-ipfs-unixfs/tree/master/packages/ipfs-unixfs-exporter#api)
  **Exports UnixFS and other DAGs from IPFS**

    - Uses the given blockstore instance to fetch an IPFS node by it's CID.
    - Returns a Promise which resolves to a UnixFSEntry.
    - ```js
      {
        type: 'file' // or 'directory'
        name: 'foo.txt',
        path: 'Qmbar/foo.txt',
        cid: CID, // see https://github.com/multiformats/js-cid
        content: function, // returns an async iterator
        unixfs: UnixFS // see https://github.com/ipfs/js-ipfs-unixfs
      }
      ```

    - If the entry is a file, `entry.content()` returns an async iterator that yields one or more `Uint8Arrays` containing the file content:
    ```js
    if (entry.type === 'file') {
      for await (const chunk of entry.content()) {
        // chunk is a Buffer
      }
    }
    ```

    - If the entry is a directory, entry.content() returns further entry objects:
    ```js
    if (entry.type === 'directory') {
      for await (const entry of dir.content()) {
        console.info(entry.name)
      }
    }
    ```

    - Entries with a `raw` codec, `CID` return raw entries:
    ```js
    {
      name: 'foo.txt',
      path: 'Qmbar/foo.txt',
      cid: CID, // see https://github.com/multiformats/js-cid
      node: Buffer, // see https://nodejs.org/api/buffer.html
      content: function, // returns an async iterator
    }
    ```

    - Entries with a `dag-cbor` codec CID return JavaScript object entries:
    ```js
    {
      name: 'foo.txt',
      path: 'Qmbar/foo.txt',
      cid: CID, // see https://github.com/multiformats/js-cid
      node: Uint8Array,
      content: function // returns an async iterator that yields a single object - see https://github.com/ipld/js-ipld-dag-cbor
    }
    ```

    - When `entry` is a file or a `raw` node, `offset` and/or `length` arguments can be passed to entry.content() to return slices of data:
    ```js
    const length = 5
    const data = new Uint8Array(length)
    let offset = 0

    for await (const chunk of entry.content({
      offset: 0,
      length
    })) {i
      data.set(chunk, offset)
      offset += chunk.length
    }

    // `data` contains the first 5 bytes of the file
    return data
    ```

    - If `entry` is a directory, passing `offset` and/or `length` to `entry.content()` will limit the number of files returned from the directory:
    ```js
    const entries = []

    for await (const entry of dir.content({
      offset: 0,
      length: 5
    })) {
      entries.push(entry)
    }

    // `entries` contains the first 5 files/directories in the directory
    ```

    - `walkPath` will return an async iterator that yields entries for all segments in a path:
    ```js
    import { walkPath } from 'ipfs-unixfs-exporter'

    const entries = []

    for await (const entry of walkPath('Qmfoo/foo/bar/baz.txt', blockstore)) {
      entries.push(entry)
    }

    // entries contains 4x `entry` objects
    ```

    - `recursive` will return an async iterator that yields all entries beneath a given CID or IPFS path, as well as the containing directory:
    ```js
    import { recursive } from 'ipfs-unixfs-exporter'

    const entries = []

    for await (const child of recursive('Qmfoo/foo/bar', blockstore)) {
      entries.push(entry)
    }

    // entries contains all children of the `Qmfoo/foo/bar` directory and it's children
    ```


### Use `js-ipfs` to construct a MerkleDAG node:
[js-ipfs (ipfs.object)](https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/OBJECT.md#ipfsobjectgetcid-options)

[js-ipfs (ipfs.dag)](https://github.com/ipfs/js-ipfs/blob/master/docs/core-api/DAG.md) **The dag API** comes to replace the object API, it supports the creation and manipulation of dag-pb object, as well as other IPLD formats (i.e dag-cbor, ethereum-block, git, etc)















### Is IPFS Daemon running?

```bash
curl -X POST http://localhost:5001/api/v0/version
```
OR (quietly):
```bash
curl -X POST http://localhost:5001/api/v0/version > /dev/null 2&>1
```
OR (catch ipfs-go version):
```bash
version=$(curl -sX POST http://localhost:5001/api/v0/version | jq -r '.Version')
```

https://flyingzumwalt.gitbooks.io/decentralized-web-primer/content/going-online/lessons/connect-your-node.html


### [Configure a node](https://docs.ipfs.io/how-to/configure-node/)
Edit: `~/.ipfs/config`
```fish
cat ~/.ipfs/config | jq
cat ~/.jsipfs/config | jq
```

### [Use an existing Private Key](https://docs.ipfs.io/how-to/use-existing-private-key/)
 * IPFS Node, Private Key: ~/.ipfs/config
 * IPFS Node, Keystore:    ~/.ipfs/keystore

Note: Not possible with `ipfs-go` at this time.

```fish
jsipfs init --private-key 'my-private-key'
jsipfs key import CustomKeyName --passin='your-pem-password' --input=./CustomKeyFile
```
> **If you already have an initialized repo** (i.e., `~/.jsipfs` is not empty), the existing key from the config file (`~/.jsipfs/config`) will be used instead of the provided one.

```js
import IPFS from 'ipfs'

const myExistingPrivateKey = 'my-private-key'
const ipfs = await IPFS.create({ privateKey: myExistingPrivateKey })
```



### [IPFS Gateways](https://docs.ipfs.io/concepts/ipfs-gateway/)
Three resolution styles exist:

 * [Path](https://docs.ipfs.io/concepts/ipfs-gateway/#path)
 * [Subdomain](https://docs.ipfs.io/concepts/ipfs-gateway/#subdomain)
 * [DNSLink](https://docs.ipfs.io/concepts/ipfs-gateway/#dnslink)



### [Mutable File System (MFS)](https://docs.ipfs.io/concepts/file-systems/#mutable-file-system-mfs)
Because files in IPFS are content-addressed and immutable, they can be complicated to edit. Mutable File System (MFS) is a tool built into IPFS that lets you treat files like you would a regular name-based filesystem — you can add, remove, move, and edit MFS files and have all the work of updating links and hashes taken care of for you.



### [File systems and IPFS](https://docs.ipfs.io/concepts/file-systems/)

MFS is accessed through the files commands in the IPFS CLI and API. The commands on the files API take the format of ipfs.files.someMethod(). These methods are used to:

 * ### [Create a directory](#create-a-directory) 
   ```js
   await ipfs.files.mkdir('/my-directory')
   await ipfs.files.mkdir('/my/nested/directory', { parents: true })
   ```

 * ### [Check directory status](#check-directory-status)
   ```js
   await ipfs.files.stat('/my-directory')
   /* returns => {
     hash: CID('QmXmJBmnYqXVuicUfn9uDCC8kxCEEzQpsAbeq1iJvLAmVs'),
     size: 60,
     cumulativeSize: 118,
     blocks: 1,
     type: 'directory'
   } */
   ```

 * ### [Add a file to MFS](#add-a-file-to-mfs)
   ```js
   let path = '/my-directory/new-file'
   await ipfs.files.write(path, content, { create: true })
   await ipfs.files.write(path, content)
   ```
   This method does not provide a return value.

 * ### [View contents of a directory](#view-contents-of-a-directory)
   To check whether the `ipfs.files.write` method has worked as expected, use the `ipfs.files.ls` method to inspect directories on the MFS by running:
   ```js
   // await ipfs.files.ls([path], [options])
   await ipfs.files.ls('/my-directory')
   ```
   This method produces an array of objects for each file or directory with properties such as `name`, `type`, `size`, `cid`, `mode`, and `mtime`.

 * ### [Copy a file or directory](#copy-a-file-or-directory)
   In the Mutable File System, like traditional systems, you can copy a file or directory to a new location while also leaving it intact at its source:
   ```js
   await ipfs.files.cp(...from, to, [options])
   ```
   This method offers _two formatting options_ for passing the from key:
      * **Existing MFS path** to a file or a directory in your node:
         (example: /my-example-dir/my-example-file.txt)
      * **IPFS address** to a file or directory:
         (example: /ipfs/QmWc7U4qGeRAEgtsyVyeW2CRVbkHW31nb24jFyks7eA2mF)

   The `to` key is the destination path in MFS, and there's an option `{ create: true }` that can be used to create parent directories that don't already exist.

   You can use this method to perform different operations including:
   ```js
   // copy a single file into a directory
   await ipfs.files.cp('/example-file.txt', '/destination-directory')
   await ipfs.files.cp('/ipfs/QmWGeRAEgtsHW3ec7U4qW2CyVy7eA2mFRVbk1nb24jFyks', '/destination-directory')

   // copy multiple files into a directory
   await ipfs.files.cp('/example-file-1.txt', '/example-file-2.txt', '/destination-directory')
   await ipfs.files.cp('/ipfs/QmWGeRAEgtsHW3ec7U4qW2CyVy7eA2mFRVbk1nb24jFyks',
   '/ipfs/QmWGeRAEgtsHW3jk7U4qW2CyVy7eA2mFRVbk1nb24jFyre', '/destination-directory')

   // copy a directory into another directory
   await ipfs.files.cp('/source-directory', '/destination-directory')
   await ipfs.files.cp('/ipfs/QmWGeRAEgtsHW3ec7U4qW2CyVy7eA2mFRVbk1nb24jFyks', '/destination-directory')
   ```




 * [Move a file or directory](#move-a-file-or-directory)
 * [Read the contents of a file](#read-the-contents-of-a-file)
 * [Remove a file or directory](#remove-a-file-or-directory)




### IPFS Peer ID:
```bash
ipfs id | jq -r .ID
```

### Publish to IPNS (to ipns://$PeerID):
```bash
PeerID=(ipfs id | jq -r .ID)
ipfs name publish $PeerID
```

### Publish to IPNS (to ipns://CustomKey):
```bash
PeerID=(ipfs id | jq -r .ID)
ipfs name publish $PeerID

exampleKey='otter.slam-dunk.gif'
Key=(ipfs key gen --type=rsa --size=2048 $exampleKey)
ipfs name publish --key=$key QmbEePAwcmxRX3oyPHS2MdAJtbcy2r8xXECNsmToKgDPq5
```
[Error: could not resolve name: "$key" is missing a DNSLink record](https://docs.ipfs.io/concepts/dnslink/)

 * Add TXT record to Domain for each media link. . 


 ### Ports:

  * [5001](https://discuss.ipfs.io/t/cannot-transfer-between-two-firewalled-machines/505/6)

  
  ### Content-Addressed Archives [(CARs)](https://car.ipfs.io/)


### [ipfs-http-client](https://www.npmjs.com/package/ipfs-http-client)
A client library for the IPFS HTTP RPC API (/api/v0/*), implemented in JavaScript. This client library implements the IPFS Core API enabling applications to change between an embedded js-ipfs node and any remote IPFS node without having to change the code. In addition, this client library implements a set of utility functions.

### [IPFS Deploy](https://agentofuser.com/ipfs-deploy/)


### [FFmpeg + IPFS](https://ffmpeg.org/ffmpeg-protocols.html#ipfs)

**InterPlanetary File System** (IPFS) protocol support. One can access files stored on the IPFS network through so called gateways. Those are http(s) endpoints. This protocol wraps the IPFS native protocols (ipfs:// and ipns://) to be send to such a gateway. Users can (and should) host their own node which means this protocol will use your local gateway to access files on the IPFS network.

If a user doesn’t have a node of their own then the public gateway dweb.link is used by default.

You can use this protocol in 2 ways. Using IPFS:

`ffplay ipfs://QmbGtJg23skhvFmu9mJiePVByhfzu5rwo74MEkVDYAmF5T`
Or the IPNS protocol (IPNS is mutable IPFS):

`ffplay ipns://QmbGtJg23skhvFmu9mJiePVByhfzu5rwo74MEkVDYAmF5T`
You can also change the gateway to be used:

`gateway`
Defines the gateway to use. When nothing is provided the protocol will first try your local gateway. If that fails dweb.link will be used.



[IPFS Content-Addressable aRchives (CAR)](https://github.com/web3-storage/ipfs-car)





## Only provide data that is relevant and worthy of YOU:
IPFS does share content that you visited but have not pinned, in order to minimize this, run a repo GC (garbage collection) often.
