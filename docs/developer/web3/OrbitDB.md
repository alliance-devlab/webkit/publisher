

## OrbitDB

```js
const IPFS = require('ipfs')
const OrbitDB = require('orbit-db')
const Log = console.log

async function main (address='') {
    // Create IPFS instance
    const ipfsOptions = { repo : './ipfs', }
    const ipfs = await IPFS.create(ipfsOptions)

    // Create OrbitDB instance
    const orbit = await OrbitDB.createInstance(ipfs)

    Log(`\nid:`)
    console.dir(orbit.id)

    Log(`\nidentity:`)
    console.dir(orbit.identity)

    Log(`\n_directConnections:`)
    console.dir(orbit._directConnections)

    Log('\n\n')

    const db = {
        log: await orbit.log(address || 'test-log'),
        feed: await orbit.feed(address || 'test-feed')
    }

    Log(db.log.address)
    Log(db.feed.address)

    let hashes = []

    hashes.push(await db.log.add({
        event: 'newMember', 
            member: {
            name: 'Bob',
            email: 'bob@my.org'
        }
    }))

    hashes.push(await db.log.add({
        event: 'newMember', 
            member: {
            name: 'Alice',
            email: 'alice@my.org'
        }
    }))

    /* hashes.forEach((hash, h) => {
        Log(`\n${hash}:`)
        console.dir(db.log.get(hash).payload.value)
    }) */

    const events = db.log.iterator({ limit: -1 })
        .collect()
        .map((e) => e.payload.value)

    console.dir(events)



    await orbit.disconnect()
}
  
main()

```


