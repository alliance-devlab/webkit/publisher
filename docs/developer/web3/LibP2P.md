# About LibP2P
`libp2p` is a _modular networking stack_. It's designed to be able to suit a variety of project needs. The configuration of libp2p is a key part of its structure. It enables you to bring exactly what you need, and only what you need.

What makes libp2p especially useful for _peer-to-peer connections_ is `connection multiplexing`. Traditionally, every service in a system opens a different connection to communicate with other services of the same kind remotely. Using IPFS, you open just one connection, and you multiplex everything on that. For everything your peers need to talk to each other about, you send a little bit of each thing, and the other end knows how to sort those chunks where they belong.

This is useful because establishing connections is usually hard to set up and expensive to maintain. With **multiplexing**, once you have that connection, you can do whatever you need on it.

`js-libp2p` acts as the composer for this modular p2p networking stack using libp2p compatible modules as its subsystems. For getting an instance of js-libp2p compliant with all types of networking requirements, it is possible to specify the following subsystems:

 * Transports
 * Multiplexers
 * Connection encryption mechanisms
 * Peer discovery protocols
 * Content routing protocols
 * Peer routing protocols
 * DHT implementation
 * Pubsub router

The libp2p ecosystem contains at least one module for each of these subsystems. The user should install and import the modules that are relevant for their requirements. Moreover, thanks to the existing interfaces it is easy to create a libp2p compatible module and use it.

Bear in mind that a `transport` and connection `encryption` module are required, while all the other subsystems are optional.


## [Concepts](https://github.com/libp2p/js-libp2p/tree/master/examples):
 * Transports
 * Protocol and Stream Muxing
 * Connection Encryption
 * Discovery Mechanisms
 * Peer and Content Routing
 * PubSub
 * NAT Traversal
 * Circuit Relay (future)
 * Naming (future)



## LibP2P Interface [Transports](https://github.com/libp2p/js-libp2p/tree/master/examples/transports)
libp2p doesn't make assumptions for you, instead, it enables you as the developer of the application to pick the modules you need to run your application, which can vary depending on the runtime you are executing. A libp2p node can use one or more Transports to dial and listen for Connections. These transports are modules that offer a clean interface for dialing and listening, defined by the interface-transport specification. Some examples of possible transports are: TCP, UTP, WebRTC, QUIC, HTTP, Pigeon and so on.

[js-libp2p-interfaces](https://github.com/libp2p/js-libp2p-interfaces/tree/master/packages/libp2p-interfaces/src/transport)

A test suite and interface you can use to implement a libp2p transport. A libp2p transport is understood as something that offers a dial and listen interface.

**Modules that impliment the interface*:
 * js-libp2p-tcp
 * js-libp2p-webrtc-star
 * js-libp2p-webrtc-direct
 * js-libp2p-websocket-star
 * js-libp2p-websockets
 * js-libp2p-utp
 * webrtc-explorer


### [js-libp2p-bootstrap](https://github.com/libp2p/js-libp2p-bootstrap)
JavaScript libp2p Implementation of the railing process of a Node through a bootstrap peer list


### [js-libp2p-mdns](https://github.com/libp2p/js-libp2p-mdns)
JavaScript libp2p MulticastDNS discovery implementation.

> Multicase DNS: "In computer networking, the `multicast DNS` (mDNS) protocol resolves hostnames to IP _addresses within small networks_ that do not include a local name server. It is a zero-configuration service"

Circuit relay protocol allows peers behind NAT boundaries to communicate, using a relay node as intermediary.

   * Local peer discovery using mDNS.
   * Peers behind NATs are unreachable?!

### [js-libp2p-websockets](https://github.com/libp2p/js-libp2p-websockets)
JavaScript implementation of the WebSockets module that libp2p uses and that implements the interface-transport interface
```js
import Libp2p from 'libp2p'
import { Websockets } from '@libp2p/websockets'
import filters from 'libp2p-websockets/filters'
import { MPLEX } from 'libp2p-mplex'
import { NOISE } from 'libp2p-noise'

const transportKey = Websockets.prototype[Symbol.toStringTag]
const node = await Libp2p.create({
  modules: {
    transport: [Websockets],
    streamMuxer: [MPLEX],
    connEncryption: [NOISE]
  },
  config: {
    transport: {
      [transportKey]: { // Transport properties -- Libp2p upgrader is automatically added
        filter: filters.dnsWsOrWss
      }
    }
  }
})
```


### [js-libp2p-webrtc-direct](https://github.com/libp2p/js-libp2p-webrtc-direct)
A WebRTC transport built for libp2p (not mandatory to use with libp2p) that doesn't require the set up a signalling server. Caveat, you can only establish Browser to Node.js and Node.js to Node.js connections.
```js
import { WebRTCDirect } from '@libp2p/webrtc-direct'
import { Multiaddr } from '@multiformats/multiaddr'
import { pipe } from 'it-pipe'
import all from 'it-all'

const ECHO_PROTOCOL = '/echo/1.0.0'
const addr = new Multiaddr('/ip4/127.0.0.1/tcp/9090/http/p2p-webrtc-direct')
const webRTCDirect = new WebRTCDirect()

const listener = webRTCDirect.createListener({
  handler: (connection) => {
    console.log('new connection opened')

    connection.newStream([ECHO_PROTOCOL])
      .then(({ stream }) => {
        void pipe(stream, stream)
      })
  },
  upgrader
})

await listener.listen(addr)
console.log('listening')

const connection = await webRTCDirect.dial(addr, {
  upgrader
})
const { stream } = await connection.newStream([ECHO_PROTOCOL])
const values = await pipe(
  [uint8arrayFromString('hello')],
  stream,
  (source) => all(source)
)
console.log(`Value: ${uint8ArrayToString(values[0])}`)

// Close connection after reading
await listener.close()
```

### [libp2p-webrtc-start](https://github.com/libp2p/js-libp2p-webrtc-star)
The webrtc-star libp2p transport and signalling server.

Packages:
 * [signalling-server](https://github.com/libp2p/js-libp2p-webrtc-star/blob/master/packages/webrtc-star-signalling-server)
 * [transport](https://github.com/libp2p/js-libp2p-webrtc-star/blob/master/packages/webrtc-star-transport)
 * [protocol](https://github.com/libp2p/js-libp2p-webrtc-star/blob/master/packages/webrtc-star-protocol)


### [Deploy a Signalling Server](https://github.com/libp2p/js-libp2p-webrtc-star/blob/master/packages/webrtc-star-signalling-server/DEPLOYMENT.md)

[Docker: libp2p/js-libp2p-webrtc-star](https://hub.docker.com/r/libp2p/js-libp2p-webrtc-star)
```yaml
version: "3.3"
services:

    js-libp2p-webrtc-star:
        image: libp2p/js-libp2p-webrtc-star
        environment:
            - VIRTUAL_HOST=${DOMAIN}
            - LETSENCRYPT_HOST=${DOMAIN}
            - VIRTUAL_PORT=9090
        networks:
            service_network:

    nginx-proxy:
        image: jwilder/nginx-proxy
        ports:
            - 443:443
            - 80:80
        container_name: nginx-proxy
        networks:
            service_network:
        volumes:
            - /var/run/docker.sock:/tmp/docker.sock:ro
            - nginx-certs:/etc/nginx/certs
            - nginx-vhost:/etc/nginx/vhost.d
            - nginx-html:/usr/share/nginx/html
        depends_on:
            - js-libp2p-webrtc-star

    nginx-proxy-letsencrypt:
        image: jrcs/letsencrypt-nginx-proxy-companion
        environment:
            NGINX_PROXY_CONTAINER: "nginx-proxy"
        networks:
            service_network:
        volumes:
            - /var/run/docker.sock:/var/run/docker.sock:ro
            - nginx-certs:/etc/nginx/certs
            - nginx-vhost:/etc/nginx/vhost.d
            - nginx-html:/usr/share/nginx/html

networks:
    service_network:

volumes:
    nginx-certs:
    nginx-vhost:
    nginx-html:
```


[hydra-booster](https://github.com/libp2p/hydra-booster)



### [Private Networking]()



Generate a key for a private network:
(source: libp2p/src/pnet/key-generator.ts)
```ts
import { randomBytes } from '@libp2p/crypto'
import { toString as uint8ArrayToString } from 'uint8arrays/to-string'
import { fromString as uint8ArrayFromString } from 'uint8arrays/from-string'

/**
 * Generates a PSK that can be used in a libp2p-pnet private network
 *
 * @param {Uint8Array} bytes - An object to write the psk into
 * @returns {void}
 */
export function generateKey (bytes: Uint8Array) {
  const psk = uint8ArrayToString(randomBytes(KEY_LENGTH), 'base16')
  const key = uint8ArrayFromString('/key/swarm/psk/1.0.0/\n/base16/\n' + psk)

  bytes.set(key)
}

export const NONCE_LENGTH = 24
export const KEY_LENGTH = 32

try {
  if (require.main === module) {
    // @ts-expect-error
    generate(process.stdout)
  }
} catch (error: any) {

}
```








## Distributed Hash Tables (DHTs)
[Distributed Hash Tables with Kademlia](https://codethechange.stanford.edu/guides/guide_kademlia.html)

A Distributed Hash Table (DHT) is a kind of data structure stored on multiple computers that aims to address this issue. 

 * _Scalable_, When more computers participate, we want the DHT to split up the elements evenly among the computers.
 * _Fault-Tolerant_, If a computer leaves the network, we want to be able to access the elements that it previously held.

There are some fundamental limitations here. If all computers leave at once, we have nowhere to store anything – thus, we will need to assume that computers leave at a slow enough rate. Intuitively, we will probably need to replicate keys across different computers so that key-value pairs will be recoverable even if some of those computers leave at once.

Goal: **Decentralized But Organized Partition of the Map**:



### [js-libp2p-kad-dht](https://github.com/libp2p/js-libp2p-kad-dht)



### [WebRTC-explorer](https://github.com/daviddias/webrtc-explorer)
[video demonstration](https://www.youtube.com/watch?v=kjwIjoENCRE)
`webrtc-explorer` is a Chord inspired, P2P Network Routing Overlay designed for the Web platform (browsers), using WebRTC as its layer of transport between peers and WebSockets for the exchange of signalling data (setting up a connection and NAT traversal). Essentially, webrtc-explorer enables your peers (browsers) to communicate between each other without the need to have a server to be the mediator.

[explorer & signalling server](https://github.com/daviddias/webrtc-explorer/tree/master/src)




### [Node Discovery Protocol v5](https://github.com/ethereum/devp2p/blob/master/discv5/discv5.md)

Node Discovery is a system for finding other participants in a peer-to-peer network. The system can be used by any node, for any purpose, at no cost other than running the network protocol and storing a limited number of other nodes' records. Any node can be used as an entry point into the network.

The system's design is loosely inspired by the Kademlia DHT, but unlike most DHTs no arbitrary keys and values are stored. Instead, the DHT stores and relays 'node records', which are signed documents providing information about nodes in the network. Node Discovery acts as a database of all live nodes in the network and performs three basic functions:

 * Sampling the set of all live participants: by walking the DHT, the network can be enumerated.
 * Searching for participants providing a certain service: Node Discovery v5 includes a scalable facility for registering 'topic advertisements'. These advertisements can be queried and nodes advertising a topic found.
 * Authoritative resolution of node records: if a node's ID is known, the most recent version of its record can be retrieved.


#### Comparison with other discovery mechanisms:

**The Node Discovery Protocol** is _designed to work on the Internet and is most useful for applications with a large number of participants spread across the Internet_. Systems such as MDNS/Bonjour allow finding hosts in a local-area network.

Systems using a rendezvous server: these systems are commonly used by desktop applications or cloud services to connect participants to each other. While undoubtedly efficient, this requires trust in the operator of the rendezvous server and these systems are prone to censorship.

The Node Discovery Protocol _doesn't rely on a single operator_ and **places a small amount of trust in every participant**. It becomes more resistant to censorship as the size of the network increases and participants of multiple distinct peer-to-peer networks can share the discovery network to further increase its resilience.

**The Achilles heel** of the Node Discovery Protocol is the process of joining the network: while any other node may be used as an entry point, such a node must first be located through some other mechanism. Several approaches including scalable listing of initial entry points in DNS or discovery of participants in the local network can be used for reasonable secure entry into the network.


#### Read more:

 * [Wire Protocol](https://github.com/ethereum/devp2p/blob/master/discv5/discv5-wire.md)
 * [Theory](https://github.com/ethereum/devp2p/blob/master/discv5/discv5-theory.md)
 * [Rationale](https://github.com/ethereum/devp2p/blob/master/discv5/discv5-rationale.md)


#### [Typescript implimentation of discV5](https://github.com/chainsafe/discv5)
```js
import { Discv5Discovery, ENR } from "@chainsafe/discv5";
import Libp2p from "libp2p";
import PeerId from "peer-id";

const myPeerId: PeerId = ...;

const bootstrapEnrs: ENR[] = [...];

const libp2p = new Libp2p({
  peerId: myPeerId,
  modules: {
    peerDiscovery: [Discv5Discovery],
  },
  config: {
    discv5: {
      enr: ENR.createFromPeerId(myPeerInfo.id),
      bindAddr: "/ip4/0.0.0.0/udp/9000",
      bootstrapEnrs: bootstrapEnrs,
      searchInterval: 30000, // wait 30s between searches
    },
  },
});
```


### Bundles:
[libP2P Bundles](https://libp2p.io/bundles/)


### Examples:
[examples](https://github.com/libp2p/js-libp2p/tree/master/examples)
In this folder, you can find a variety of examples to help you get started in using js-libp2p, in Node.js and in the Browser. Every example has a specific purpose and some incorporate a full tutorial that you can follow through, helping you expand your knowledge about libp2p and p2p networks in general.

 * [chat](https://github.com/libp2p/js-libp2p/tree/master/examples/chat)
 * [connection-encryption](https://github.com/libp2p/js-libp2p/tree/master/examples/connection-encryption)
 * [discovery-mechanisms](https://github.com/libp2p/js-libp2p/tree/master/examples/discovery-mechanisms)
 * [in-the-browser](https://github.com/libp2p/js-libp2p/tree/master/examples/libp2p-in-the-browser)
 * [peer & content routing](https://github.com/libp2p/js-libp2p/tree/master/examples/peer-and-content-routing)
 * [private networking](https://github.com/libp2p/js-libp2p/tree/master/examples/pnet)
 * [muxing (protocol & stream)](https://github.com/libp2p/js-libp2p/tree/master/examples/protocol-and-stream-muxing)
 * [pubsub](https://github.com/libp2p/js-libp2p/tree/master/examples/pubsub)
 * [transports](https://github.com/libp2p/js-libp2p/tree/master/examples/transports)
 * [webrtc-direct](https://github.com/libp2p/js-libp2p/tree/master/examples/webrtc-direct)



## [LibP2P Examples (javascript)](https://github.com/libp2p/js-libp2p/tree/master/examples)

 * [Private Networking](https://github.com/libp2p/js-libp2p/tree/master/examples/pnet)
   - This example shows how to set up a private network of libp2p nodes.


 * [Boostrap new LibP2P peers](https://www.npmjs.com/package/libp2p-bootstrap)

 ```js
const Libp2p = require('libp2p')
const Bootstrap = require('libp2p-bootstrap')
const TCP = require('libp2p-tcp')
const { NOISE } = require('libp2p-noise')
const MPLEX = require('libp2p-mplex')

let options = {
    modules: {
        transport: [ TCP ],
        peerDiscovery: [ Bootstrap ],
        streamMuxer: [ MPLEX ],
        encryption: [ NOISE ]
    },
    config: {
        peerDiscovery: {
            [Bootstrap.tag]: {
                list: [ // a list of bootstrap peer multiaddrs to connect to on node startup
                  "/ip4/104.131.131.82/tcp/4001/ipfs/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                  "/dnsaddr/bootstrap.libp2p.io/ipfs/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN",
                  "/dnsaddr/bootstrap.libp2p.io/ipfs/QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa" 
                  ],
                  interval: 5000 // default is 10 ms,
                  enabled: true
            }
        }
    }
}

async function start () {
  let libp2p = await Libp2p.create(options)

  libp2p.on('peer:discovery', function (peerId) {
    console.log('found peer: ', peerId.toB58String())
  })

  await libp2p.start()

}

start()
 ```


 * [js-libp2p-tcp](https://github.com/libp2p/js-libp2p-tcp)
   JavaScript implementation of the TCP module for libp2p. It exposes the interface-transport for dial/listen. libp2p-tcp is a very thin shim that adds support for dialing to a multiaddr. This small shim will enable libp2p to use other transports.

   ```js
   import { TCP } from '@libp2p/tcp'
   import { multiaddr } from '@multiformats/multiaddr'
   import {pipe} from 'it-pipe'
   import all from 'it-all'

   // A simple upgrader that just returns the MultiaddrConnection
   const upgrader = {
   upgradeInbound: maConn => maConn,
   upgradeOutbound: maConn => maConn
   }

   const tcp = new TCP({ upgrader })

   const listener = tcp.createListener({
   handler: (socket) => {
      console.log('new connection opened')
      pipe(
         ['hello', ' ', 'World!'],
         socket
      )
   }
   })

   const addr = multiaddr('/ip4/127.0.0.1/tcp/9090')
   await listener.listen(addr)
   console.log('listening')

   const socket = await tcp.dial(addr)
   const values = await pipe(
   socket,
   all
   )
   console.log(`Value: ${values.toString()}`)

   // Close connection after reading
   await listener.close()
   ```