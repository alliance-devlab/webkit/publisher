
### JSON-LD Aliasing:

```json
"@context": {
  "id": "@id",
  "type": "@type",
  "cid": "@id"
}
```



### JSON-LD Language Coercion
```json
"@context": {
  "@language": "jP"
}
```












[Linked Data Fragments](https://linkeddatafragments.org/specification/linked-data-fragments/)

[JSON-LD 1.1 Specs](https://www.w3.org/TR/json-ld/)

[Search for Schemas](https://schema.org/docs/search_results.html?q=$query)

### Schemas (schema.org):
 * [DigitalDocument](https://schema.org/DigitalDocument)
 * [ImageObject](https://schema.org/ImageObject)
 * [AudioObject]()
 * [VideoObject]()

 * [Place](http://schema.org/Person)
 * [Person](http://schema.org/Person)

 * [Legislation](https://schema.org/Legislation)
 * [Book](http://schema.org/Book)
 * [MusicRecording](http://schema.org/MusicRecording)

### [Example JSON-LDs](https://jsonld.com/organization/)

### JSON-LD Schemas
 * [Steal Our JSON-LD](https://jsonld.com/)

### Javascript 
 * #### [jsonld.js](https://www.npmjs.com/package/jsonld)
   This library is an implementation of the JSON-LD specification in JavaScript.

   JSON, as specified in [RFC7159](http://tools.ietf.org/html/rfc7159), is a simple language for representing objects on the Web. Linked Data is a way of describing content across different documents or Web sites. Web resources are described using IRIs, and typically are dereferencable entities that may be used to find more information, creating a "Web of Knowledge". JSON-LD is intended to be a simple publishing method for expressing not only Linked Data in JSON, but for adding semantics to existing JSON.

 * #### [jsonld-signatures](https://www.npmjs.com/package/jsonld-signatures)
   A Linked Data Signature proof is created (or verified) by specifying a signature suite and a proof purpose.

The signature suite performs the cryptographic operation required to sign (or verify) a digital signature and includes information in a proof such as the verificationMethod identifier, the proof's controller, and the date the proof was created.

[reference of Schema.org converted to JSON](https://github.com/geraintluff/schema-org-gen/tree/master/schema.org "(9 years old and not in JSON-LD format)")

### Schema Validation systems:
 * [Ajv JSON schema](https://ajv.js.org/guide/why-ajv.html)

#### JSON schema formats:
 * JSON Schema
 * JSON Type Definition:
   - [specs](https://datatracker.ietf.org/doc/html/rfc8927)
   - [Learn JSON Typedef in 5 Minutes](https://jsontypedef.com/docs/jtd-in-5-minutes/)


### [RDF Dataset Canonicalization](https://json-ld.github.io/rdf-dataset-canonicalization/spec/)
RDF [RDF-CONCEPTS] describes a graph-based data model for making claims about the world and provides the foundation for reasoning upon that graph of information. At times, it becomes necessary to compare the differences between sets of graphs, digitally sign them, or generate short identifiers for graphs via hashing algorithms. This document outlines an algorithm for normalizing RDF datasets such that these operations can be performed.


### [N-Quads](https://dvcs.w3.org/hg/rdf/raw-file/default/nquads/index.html)
N-Quads is a line-based, plain text format for encoding an RDF dataset.


[About Schema.org presentation](https://schema.org/docs/kickoff-workshop/sw1109_Vocabulary.pdf)



[JSON-LD Context: 'person'](https://json-ld.org/contexts/person.html)

[JSON-LD Best Practices](https://w3c.github.io/json-ld-bp/)


[jsonld](https://www.npmjs.com/package/jsonld)


[is-my-json-valid](https://www.npmjs.com/package/is-my-json-valid)


