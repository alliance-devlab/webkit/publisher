



# [BSON]()


# CBOR: [Concise Binary Object Representation](https://cbor.io/)
“_The Concise Binary Object Representation_ (CBOR) is a data format whose design goals include the possibility of extremely small code size, fairly small message size, and extensibility without the need for version negotiation.”

 * [cbor-js](https://www.npmjs.com/package/cbor-js)
   ```js
   var initial = { Hello: "World" };
   var encoded = CBOR.encode(initial);
   var decoded = CBOR.decode(encoded);
   ```

   ```js
   var websocket = new WebSocket(url);
   websocket.binaryType = "arraybuffer";
   //...
   websocket.onmessage = function(event) {
      var message = CBOR.decode(event.data);
   };
   //...
   websocket.send(CBOR.encode(message));
   ```

 * [cbor package](https://www.npmjs.com/package/cbor)
 * [node-cbor package](https://github.com/hildjj/node-cbor)
Encode and decode CBOR documents, with both easy mode, streaming mode, and SAX-style evented mode.


# [Protocolo Buffers]()

