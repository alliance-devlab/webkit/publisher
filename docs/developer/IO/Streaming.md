### HLS (Http Live Streaming):

[Fission.codes experiment with HLS](https://fission.codes/blog/experimenting-with-hls-video-streaming-and-ipfs/)


[medium.com](https://medium.com/revotic/streaming-with-ipfs-2145e6df5a4e "How IPFS Streaming Works")

![IPFS Streaming](../assets/Streaming.IPFS.png)


[hls.js](https://www.npmjs.com/package/hls.js "HTTP Live Streaming")

[video.js](https://www.npmjs.com/package/video.js)

[mux.js](https://www.npmjs.com/package/mux.js "Lightweight utilities for inspecting and manipulating video container formats.")