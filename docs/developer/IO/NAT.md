

[Connect to local NAT address](http://10.1.10.1)
 * Login using admin username/password.


[NAT configuration for IPFS](https://docs.ipfs.io/how-to/nat-configuration/#nat-configuration)

While the NAT is usually transparent for outgoing connections, listening for incoming connections requires some configuration. The router listens on a single public IP address, but any number of machines on the internal network could handle the request. To serve requests, your router must be configured to send certain traffic to a specific machine. You can configure your router to allow these connections by either of the following sections.

[UPnP, Universal Plug-n-Play](https://www.upguard.com/blog/what-is-upnp)

The UPnP service becomes dangerous if it establishes connections with devices that are infected with malware. Such connections make DDoS attacks possible.

But when UPnP allows safedevices to connect, the established network is safe. So the original intention of UPnP technology is safe. It only becomes dangerous when infected devices are involved.

