## Networking/Ports:
Douglas Comer explains, "_We will imagine that each machine contains a set of abstract destination points_ called **protocol ports**. Each protocol port is identified by a positive integer."

The **TCP/IP protocol** suite uses these imagined destination endpoints to distinguish among multiple destination processes in a single host computer. Think of an office phone number extension. Connection-oriented services such as e-mail use **TCP** to establish reliable communication streams between hosts while other applications and services might use **UDP** to transport messages between systems.

In either case the _Source Port_ and _Destination Port_ are part of the **network addressing** used to establish the connection. IP receives datagrams and delivers them to the appropriate processing software based on the _connection type_ and _protocol port number_.
