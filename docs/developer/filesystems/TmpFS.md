## tmpfs
tmpfs is the name of a temporary file system used on many Unix-like filesystems. It is mounted and structured like a disk-based filesystem, but resides in volatile memory space, similar to a RAM disk. Mount options for tmpfs:

```fish
mkdir ~/temp/tmpfs.testing
chmod 777 ~/temp/tmpfs.testing
mount -F tmpfs -o size=50 swap ~/temp/tmpfs.testing
```

[works on Mac](https://gist.github.com/koshigoe/822455)

### Watch directory for changes:
[https://nodejs.org/api/fs.html#fswatchfilename-options-listener]()
[Chokidar](https://www.npmjs.com/package/chokidar)
 * MacOS, FSEvents: https://en.wikipedia.org/wiki/FSEvents