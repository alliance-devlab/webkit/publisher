## Initializing new Git repo:

Command line instructions
You can also upload existing files from your computer using the instructions below.

#### Git global setup
```bash
git config --global user.name "NC Alliance"
git config --global user.email "nevadacountyalliance@protonmail.com"
```

#### Create a new repository
```bash
git clone git@gitlab.com:alliance-devlab/webkit/publisher.git
cd publisher
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

#### Push an existing folder
```bash
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:alliance-devlab/webkit/publisher.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

#### Push an existing Git repository
```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:alliance-devlab/webkit/publisher.git
git push -u origin --all
git push -u origin --tags
```

#### Date of repo creation:
```bash
git log --reverse
```
[see](https://www.ryadel.com/en/github-first-commit-date-git/)


#### Merge Requests
[Merge Request via CLI](https://dev.to/farnabaz/gitlab-create-merge-requests-from-cli-c36)