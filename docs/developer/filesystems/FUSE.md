## [Filesystem in Userspace](https://en.wikipedia.org/wiki/Filesystem_in_Userspace)

## [FUSE](https://github.com/libfuse/libfuse)
FUSE (Filesystem in Userspace) is an interface for userspace programs to export a filesystem to the Linux kernel. The FUSE project consists of two components: the fuse kernel module (maintained in the regular kernel repositories) and the libfuse userspace library (maintained in this repository). libfuse provides the reference implementation for communicating with the FUSE kernel module.

A FUSE file system is typically implemented as a standalone application that links with libfuse. libfuse provides functions to mount the file system, unmount it, read requests from the kernel, and send responses back. libfuse offers two APIs: a "high-level", synchronous API, and a "low-level" asynchronous API. In both cases, incoming requests from the kernel are passed to the main program using callbacks. When using the high-level API, the callbacks may work with file names and paths instead of inodes, and processing of a request finishes when the callback function returns. When using the low-level API, the callbacks must work with inodes and responses must be sent explicitly using a separate set of API functions.


### [Fuse Native](https://github.com/fuse-friends/fuse-native)
Multithreaded FUSE bindings for Node JS.

 * [Fuse.configure(cb)](https://github.com/fuse-friends/fuse-native#fuseconfigurecb)
    Configures FUSE on your machine by enabling the FUSE kernel extension. You usually want to do this as part of an installation phase for your app. Might require sudo access.
 * [Fuse.isConfigured(cb)](https://github.com/fuse-friends/fuse-native#fuseisconfiguredcb)
    Returns true if FUSE has been configured on your machine and ready to be used, false otherwise.



#### Projects that use FUSE:
 * [Brig](https://github.com/sahib/brig)
   Brig is a distributed & secure file synchronization tool with version control. It is based on IPFS, written in Go and will feel familiar to git users.


