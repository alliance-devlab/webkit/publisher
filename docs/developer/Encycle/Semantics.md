

### Holonym:Meronym

The _part-whole relation between nouns_ is generally considered to be a semantic relation, called _meronymy_ (from the Greek meros, part), comparable to synonymy, antonymy, and hyponymy. The relation has an inverse: _holonym_.





Music:Discography

   `Discography` is a kind of 


MATCH (c:Collection)
WHERE c.creator = "Animal Collective"
RETURN (c.content)





"Music by Animal Collective."

"Discography by Animal Collective"

(content: Content({ kind: 'Music'}))-[:BY]-(creator:Creator {name: "Animal Collective"})
   =>
(creator: "Animal Collective")-[:CREATED]-(collection)



:BY
:CREATED


:SPECIFIES
:INHERITS-FROM