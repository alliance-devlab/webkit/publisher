---
home: 'https://www.yarrow.love'
journal:
  entries:
    - 2022_March_5th
    - 2022_March_7th

portfolio:
  gallery:
    - sculptureA
notice: '/notice'
---


```js
let site = {
  config: {
    site: {
      name: 'Alliance Buildkit Skeleton'
      domain: {
        http: false,
        https: 'https://yarrow.love',
        //? ipfs,ipns
      } 
    }
  },
  content: [
    public: {}
  ],
  keys: {},
  authors: {}
}
```


```js
let home = {
  route: '/'
  content: [{
    component: 'Hero',
    id: 'Home',
    content: {
      md: ""
    }
  },{
    component: 'Gallery',
    id: 'Documents',
    class: 'open_gallery',
    src: ['imageA.img','imageB.img']
    content: {
      md: ""
    }
  },{
    component: 'Content',
    id: 'Notice',
    content: {
      md: ""
    }
  }]
}
```



```js
let sitemap = {
  home: 'https://www.yarrow.love',
  journal: {
    entries: {
      '2022_March_5th': {},
      '2022_March_7th': {}
    }
  },
  portfolio: {
    gallery: {
      sculptureA: {}
    }
  },
  notice: {}
}

```