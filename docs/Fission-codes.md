## Fission: Multiple Accounts per Device:
You may create multiple Fission accounts, but you'll need a **unique email address and username** for each one. You'll also need to use Browser Profiles to be able to access them at the same time on the same machine, as the keys that grant access are stored in the browser.



## [Fission Configuration](https://guide.fission.codes/developers/cli/fission-yaml)
 * 'ignore': "Ignore is a list of files you don't want fission app publish to publish. For example, you might add commonly ignored files and secrets."
 * 'root_proof': "The UCAN that was used to link from another device or browser. null if the account was created on this device."
 * 'server_did': "Server DID is the identity of the Fission server that authenticates your requests when using the Fission CLI."
 * 'peers': "We configure your machine to directly connect to our servers. The peers are the IP addresses and fingerprints of our servers."
 * 'signing_key_path': "The path to the key used to sign requests made when using the Fission CLI."

## Fission App Configuration:
```yaml
ignore: []
url: junior-angular-tulip.fission.app
build: ./dist
ignore:
 - node_modules
 - *.psd
```

## [Custom Domains](https://guide.fission.codes/developers/custom-domains)

 * [Adding DNS Records for Fission custom domains.](https://guide.fission.codes/developers/custom-domains/control-own-dns)



## [WebNative SDK](https://guide.fission.codes/developers/webnative)