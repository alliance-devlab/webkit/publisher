### Command Line Installation
Install Atom 1.22 or newer

In the terminal, install the package via apm:
```
apm install teletype
```

#### This package is powered by three main components:
teletype-crdt: The string-wise sequence CRDT that enables peer-to-peer collaborative editing.
teletype-server: The server-side application that facilitates peer discovery.
teletype-client: The editor-agnostic library that manages the interaction with other clients.
