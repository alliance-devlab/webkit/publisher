

## **Design/Build Manual** for Webkit Content:


### Rule: "Every directory is a route."

 * Route inherits its path from of the directory,
   - unless `route` is set on route config.
 * Suffixes denoting components (eg, '.blog', '.store') are stripped
 * Except: Directories ending in '.ignore' and '.draft' are ignored.


### Rule: "Every route must have an entrypoint."
 * Entrypoint must be unambiguous `.md` file, <br>satisfied in any one of these cases:
   - Single `.md` file present: Call it whatever you like.
   - Multiple markdown files: Suffix entrypoint with `.index.md`

 * Require: Not more than one `*index.md`


### Rule: "Configuration is relative."

#### Route-specific configuration:

 * A route's config is inherited by its children,
 unless that data is wrapped by the `protected` keyword, which is reserved for blocking config inheritance on specific branches of data.

 * Thus, config written at the root of the `public` directory will be available to to all site routes.

#### Page-specific configuration:

 * Configuration is applied to a single page by giving it the same name as the content it is to be applied to, for example to configure only `profile.md` without affected descendent pages, write configuration to the `profile.yml` file. Simple enough?

 * Metadata on media and content is essentially written the same way; to assign metadata to the file `butteryfly.pdf` add a `butterfly.yml` to the route.
   - if multiple `butterful.*` files exist, make metadata unambigous by prefixing `.yml` by the full filename to make: `butterfly.pdf.yml`
   