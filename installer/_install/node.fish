#! /bin/fish


set bin $HOME/.local/bin
set lib $HOME/.local/lib/webkit

set node_v "v16"

nvm install $node_v > /dev/null 2>&1 #--lts
nvm use $node_v; echo
#? > /dev/null 2>&1

#? printf "# Installed Node.js $node_v"

cd /usr/local/bin
if test -f ./node;
   sudo rm node
end
if test -f ./npm;
   sudo rm npm
end

cd $bin
sudo ln -s $(which node) ./node
sudo ln -s $(which npm) ./npm

cd /bin
if test -f ./node; sudo rm ./node; end

sudo ln -s $(which node) ./node

set node_bin $HOME/.nvm/versions/node/(node --version)/bin
set -Ux NODE_PATH $HOME/.nvm/versions/node/(node --version)/lib/node_modules
npm config set prefix $HOME/.nvm/versions/node/(node --version) > /dev/null 2>&1
fish_add_path $node_bin


npm install --silent -g yarn > /dev/null

echo
set_color -o green
printf "  # Installing Node command-line tools:\n"
set_color bryellow
printf "    • 'yarn': Node package manager\n"

npm install --silent -g @vue/cli
set_color bryellow
printf "    • 'vue':  Front-end javascript framework\n"


cd $lib
set webkit_v (jq -r '.version' <package.json)
set_color -o brgreen
printf "\n  # Installing Webkit v$webkit_v\n" ''
set_color normal

cd $lib
$node_bin/yarn
