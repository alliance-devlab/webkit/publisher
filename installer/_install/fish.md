
### Upgrade your CLI experience now:

We highly recommend that you upgrade your shell 🐚 (the terminal interface you're using) by installing *Fish Shell*, the "Friendly Interactive Shell".

**To read more about 'fish':**
```
 • Run: `webkit docs fish`
 • https://fishshell.com/
```
