
### Webkit login

The Webkit login experience is powered by a super simple API using Restify and NeDB, which essentially store the login password in memory. If the API is reachable, then `$publisher` is logged-in. The API requires the account public.pgp key, in order to access the API, which ensures that only localhost can access passphrase.



```sh
.
├── publishers
│   └── steward
│       ├── credentials
│       └── .keys
│           ├── private.pgp
│           └── public.pgp
```
