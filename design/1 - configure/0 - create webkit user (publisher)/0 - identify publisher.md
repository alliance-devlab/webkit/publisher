#! webkit new publisher

Welcome to Webkit!

Let's start by creating a new publisher account for you.

#### Give your publisher account a unique id: ${USER}

### What should we call you?
${git config user.name || "Margaret Rowland" capitalize(${gen name} ${gen name})}

Thanks, Margaret!

Webkit will now create a initialize a password manager for `$publisher`.

It is highly recommended to protect your credentials with password encryption. (If you wish not to secure this Webkit publisher account with a password, just hit ENTER.)

### Create master password for `$publisher`: 

 * Generating GPG keys.
 * Logging in.

@login: "Welcome, Margaret!"
