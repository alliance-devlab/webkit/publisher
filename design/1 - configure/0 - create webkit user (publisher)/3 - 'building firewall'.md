
### Is Uncomplicated Firewall installed?
```
which ufw
```

### Check status:
```
sudo ufw status
```

### If ufw running: Firewall is already running.
   Would you like Webkit to secure the firewall? If yes, return;
### Otherwise, secure.

### Configure firewall:
["Disallowing incoming connections"]()
```
sudo ufw default deny incoming
```

["Allowing outgoing connections"]()
```
sudo ufw default allow outgoing
```

   ### Check UFW config to allow IPv6 connections.
   If not, ```sudo cat /etc/default/ufw | grep '^IPV6=yes$'```
   ```
   sudo echo "IPV6=yes" >> /etc/default/ufw
   ```


### Do you need to open any services for incoming connections?

   #### Yes?
   ```
   sudo ufw app list
   ```

   (create selection menu with item for each app in appList + SSH)

   ### If Open SSH,
   ```
   sudo ufw allow 22
   ```

   ### For each enabled service:
   ```
   sudo ufw allow $service
   ```

### Allow incoming connections by IPFS peers?
This is required for hosting IPFS data from your local node. Notice: IPFS is only for public data, and as such all content and media saved into the `content/public` folder will be available for anyone in the world.

For this reason, Webkit explicitly distinguishes `public` and `private` content, as you will see.

So it is recommended to allow IPFS connections, knowing full well that all content in the `content/public` directory of every Webkit project will be transparent and _public_. For sensitive material, always utilize the `content/private` folder.

```
sudo ufw allow 4001
```

#### Port explaination:
 * [4001](Connecting IPFS peers)        //Allow incoming
 * [5001](internal API for ipfs daemon) //Deny incoming
 * [8080](Browser Application)          //Deny incoming

### Show Firewall Configuration:
```
sudo ufw status