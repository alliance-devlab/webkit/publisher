### Is installed?
 * git
 * gpg v2   //? (rnpkeys)
 * fish ? nvm-fish : nvm
 * node (nvm lts)
   - npm i -g yarn
 * ipfs
   - cli
   - ? desktop
 * ufw
 


### Is firewall running?
```
sudo ufw status
```
!== "Status: inactive"

### Is IPFS Daemon running?
```bsh
curl -X POST http://localhost:5001/api/v0/version > /dev/null 2&>1
```
