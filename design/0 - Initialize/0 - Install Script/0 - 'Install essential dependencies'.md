### Initialization requires:
 * wget curl
 * yq
 * git


### Install:
 * wget curl
 * yq
 * git
 * ufw
 * ?fish
 * node
   - configure node
 * ?codium "Write Webkit content in open-source VS Codium.
 * ?yt-dlp "Download Videos from the Internet"

### Is installed?
 * ascii-image-converter
 * git
 * yq
 * gpg v2   //? (rnpkeys)
 * fish ? nvm-fish : nvm
 * node (nvm lts)
   - npm i -g yarn
 * ipfs
   - cli
   - ? desktop
 * ufw
 * glow ("Markdown in commandline")
 



## Load Webkit:
Purple("Bootloading Webkit")
   * install --quietly git ascii-image-converter
   * mkdir -p ~/.local/bin
   * mkdir -p ~/.local/lib/webkit
   * cd ~/.local/lib/webkit
   - If Webkit repo is testing (not public):
      - USE DEPLOY KEYS until repo goes public.
   - Else git clone https://gitlab.com/alliance-devlab/webkit/publisher



## Initiating Webkit Publisher . . .

#### Install --quietly 'yq'
```which yq```
```
//?{ cwd: ~/.local/bin }

sudo wget -qO ./yq https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64
sudo chmod a+x yq
```


#### Install --quietly 'ascii-image-converter'
```
which ascii-image-converter

echo 'deb [trusted=yes] https://apt.fury.io/ascii-image-converter/ /' | sudo tee /etc/apt/sources.list.d/ascii-image-converter.list

sudo apt update
sudo apt install -y ascii-image-converter
```







#### Glow (Markdown TUI):
```
which snap
sudo apt install snapd

sudo snap install glow

```


#### Git:
```
which git
git --version

sudo apt install git
```

#### Uncomplicated Firewall:
```
which ufw

sudo apt install -y ufw
```







### VS Codium:
```
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg
echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list
sudo apt update
sudo apt install -y codium
```
