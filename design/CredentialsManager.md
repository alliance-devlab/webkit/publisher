

### Data structures:
`let encrypted = BSON.parse(fs.readFileSync('store.bson')`:
```json
{
   "encrypted": {
      "owner": "steward",
      "created":  1651997120544,
      "modified": 1651910768861,
      "data": binary
   }
}
```

`let store = BSON.parse(GPG.decrypt(encrypted.data)`:
```json
{
   "salt": crypto.randomBytes(64),
   "credentials": {
      "work/SSH:steward@webkit.ipfs": binary,
      "personal/steward@protonmail.com": binary
   }
}
```

let index = Object.keys(store.credentials)
log(index) // ['work/SSH:steward@webkit.ipfs','personal/steward@protonmail.com']


```js



const fs = require('fs')

let file = './credentials.bson'
let encrypted = BSON.parse(fs.readFileSync(file))
let { owner, created, modified, data } = encrypted

// Decrypt 'encrypted.data', unwrap credentials:
let store = BSON.parse(GPG.decrypt(encrypted.data))
let { salt, credentials } = store
let keys = Object.keys(credentials)
// keys: List of credential identifiers, eg: 'webkit/clients/steward/client.key', 'personal/steward@protonmail.com'

```

### Decryption Process:
```js
const Credentials = new CredentialsManager()
let store = Credentials.loadStore(`./path/to/store.bson`, pw)
let id = Credentials[store].findOne('steward@webkit.service/client.key')
Credentials.reveal(key, jqQuery) // Prints JSON object with jq.
```

#### _What just happened?_
1. Read and parse `.bson` credential store.
2. Decrypt `encrypted.data` with locally trusted GPG key.
3. Read `Object.keys(credentials)`; Search `keys` for credential and get `key`.
4. Decrypt credentials[key] with cypher.


The 'salt' is used to obfuscate credential values.



