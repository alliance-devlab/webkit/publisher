<p style="color: red;">//! Early stage development & testing! Curious developers only.
</p>

!["Hello Webkit"](/assets/graphics/HelloWebkit.png)

## **Webkit** by _Great Works Alliance_

### _Unstoppable_ **Communication Power**.<br>

Webkit delivers _component-oriented_ web design (built on Vue.js) to a broad audience of content creators and publishers by use of our especially designed _with love_ **Markdown++** notation and preprocessor. Webkit allows contributors to utilize Web Components using an [advanced shortcode syntax]() that is simple and easy on the eyes, yet powerful. Webkit orchestrates the build process when contributors produce new content and distributes site media on [Web3 Infrastucture](), so community media is resilient.

> "_**Flexibility**_ is the ability to reconfigure according to changing environments and retain their goals while changing their components, sometimes bypassing blocking points of communication channels to find new connections. _**Scalability**_ is the ability to expand or shrink in size with little disruption. _**Survivability**_ is the ability of networks, because they have no single centre and can operate in a wide range of configurations, to withstand attacks on their nodes and codes because the codes of the network are contained in multiple nodes that can reproduce the instructions and find a new way to perform." ―<u>Communication Power</u>, Manuel Castelles

### **Webkit** is built for _unstoppable_ publishing communities.


!["Run Webkit"](https://media0.giphy.com/media/xsF1FSDbjguis/giphy.gif)

**Equip your publishing community with Web3-authoring superpowers.** Webkit provides a powerful and easy-to-operate CLI, leveraging the best that [Web2]() and [Web3](/docs/web3.md) have to offer. If you are an author, an investigative journalist, a community organizer, or a web designer who wants to supercharge your community's Communication Power, _continue reading . . ._

<br>

#### Now that you're interested . . . 
### Read the **Webkit Documentation**:

 * [**Fundamentals** _for writers_.](/docs/fundamentals)

 * [**Quickstart** _for publishers_.](/docs/quickstart.md)

 * [**Webkit Manual**: Building Webkit Content](/docs/directories+files.md)

 * [**The Publisher's Guide**: Web Stewardship](/docs/guide.md)

<br>

### Webkit Quickstart: _for publishers in a rush_:

!["Run Webkit"](./assets/gifs/godspeed.gif)

To install Webkit now, simply run the following script (Ubunty only at this time):
```bash
wget -qO - "https://gitlab.com/alliance-devlab/webkit/publisher/-/raw/main/installer/launch" | bash
```


This installs the `webkit` CLI tool, which runs from the command line. To use it, open a terminal, type `webkit` and hit enter. The `webkit` command also accepts subcommands and arguments, such as: ```webkit create otter_website```, which will make a new folder called `otter_website` in the current directory.

#### Read the full [Installation Instructions](#Install).

#### Launch Webkit Demo Project:

```webkit launch --demo```

This command will:

 1. Install dependencies necessary for Webkit
 2. Walk you through setting up credentials for Web3 infrastructure (to make your content unstoppable)
 3. Create a `public` repo, where your content will be prepared and archived.
 4. Copy example files that demonstrate the file and directory structure of Alliance Webkit sites.




## What's under the hood?

#### Web3
https://web3.foundation/

#### IPFS
https://ipfs.io/

#### Web3.storage
https://web3.storage/

#### Fission.codes
https://fission.codes/

