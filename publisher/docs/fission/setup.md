### Create a Fission Account:

 * Visit [https://auth.fission.codes/](https://auth.fission.codes/)
 * Create an account (simply an `email` and `username`)

### Integrate Fission account with Webkit:
 
  * Revisit [https://auth.fission.codes/](https://auth.fission.codes/)
    - Now you should see: "**Authenticated as** _username_"
    - Leave this browser tab open while integrating with Webkit:
  * Provide your credentials to Webkit.
    - **Notice:** Webkit has a built-in credentials manager secured locally by your publisher keys, so all your credentials are safe and sound.

**To read more about Fission:**
 * Run: `webkit docs fission`
 * https://guide.fission.codes/
