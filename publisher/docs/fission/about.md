### Fission is a Web3 framework 
_Fission_ provides **advanced capabilities** on the IPFS network:
   * Human-readable, permanent links on the distributed Web.
   * Web3 account data (public & private): https://drive.fission.codes
   * Deploy decentralized websites and P2P applications.

Some capabilities provided by Fission may be satisfied by alternative means:
   * See: `custom-domain` driver for readable perma-links on custom domain.
      - Integrates with either Epik or Linode API integration for DNS configuration of IPNS content.
   * See: `deploy/surge.sh` driver for details on Web2 publishing.

Utilizing _advanced P2P capabilities_ **will require a Fission account**.

**To read more about Fission:**
 * https://guide.fission.codes/
 * Run: `webkit docs fission`
