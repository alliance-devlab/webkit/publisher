module.exports = {
   email: email => /^[^@ ]+@[^@ ]+\.[^@ \.]+$/.test(email),

   password: {
      requireLetterAndNumber: (value) => {
         return (/\w/.test(value) && /\d/.test(value))
            ? true
            : 'Password need to have at least a letter and a number'
      }
   }
}
