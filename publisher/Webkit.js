const Command = require('commander').Command
const Inquire = require('inquirer')

const fs = require('fs')
const sh = require('child_process')
const log = console.log
const logDir = console.dir

const Path = require('path')
const Color = require('chalk')
const { hot_pink } = require('../utils/colors.js');
const Gradient = require('gradient-string')
const Debug = require('debug')
const debug = Debug('cli');
const waitForEnter = require('../utils/waitForEnter')

const Mime = require('mime')
const Json = JSON
const Bson = require('bson')
const Markdown = require('front-matter')

const env = process.env;
const HOME = env.HOME
const USER = HOME.split('/').pop()

const WEBKIT = {}
WEBKIT.bin = parentDirectory(fs.realpathSync(process.argv[1]))
WEBKIT.dir = parentDirectory(WEBKIT.bin)
WEBKIT.config = Path.join(HOME, '.config/webkit')
WEBKIT.version = sh.execSync("jq -r '.version' <package.json", {cwd: WEBKIT.dir}).toString().trim()
//? Export environmental variables? Or attach to process.env?
debug(WEBKIT)


//* 1. Are there any publisher accounts?

function isInitiated () {
   let publishersDir = Path.join(WEBKIT.config, 'publishers')
   if (!fs.existsSync(publishersDir)) fs.mkdir(publishersDir, { recursive: true })
   let publishers = fs.readDirSync(publishersDir)
   return Boolean(publishers.length)
}

if (!isInitiated) 



//* 2. Is the publisher account logged in?

const loginService = 'http://localhost:9700/hello/yarrow'
sh.execSync(`curl --output /dev/null --silent --head --fail "${loginService}"`)









