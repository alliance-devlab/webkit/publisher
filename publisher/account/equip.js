const log = console.log
const fs = require('fs')
const sh = require('child_process')

const Path = require('path');
const Spin = require('ora')
const Color = require('chalk');
const { hot_pink } = require('../utils/colors.js');
const waitForEnter = require('../utils/waitForEnter')

const Inquirer = require('inquirer');
const { Console } = require('console');


function parentDir (dir) {
   let tree = dir.split('/'); tree.pop()
   return tree.join('/')
}

function isDirEmpty(dirname) {
   return fs.promises.readdir(dirname).then(files => {
       return files.length === 0;
   });
}

const HOME = process.env.HOME
const configDir = Path.join(HOME, '.config')
const fissionDir = Path.join(HOME, `.fission`)
const webkitDir = Path.join(fs.realpathSync(process.argv[1]), '../../..')




async function equip (publisher) {
   log(Color.hex(hot_pink)(`\nLet's equip your publisher account with Web3 capabilities.`));
   let { confirm } = await Inquirer.prompt([{
      type: 'confirm',
      name: 'confirm',
      message: "Interested?",
      default: true
   }])
   if (!confirm) return false

   let capabilities = []
   let fission = await equipFission(publisher)
   if (fission) capabilities.push({ fission })

   //?log(Color.yellow(`equip().fission:`))
   //?console.dir(fission)
   //?console.dir(capabilities)

   return (capabilities.length > 0) ? capabilities : false
}




async function setupFission (publisher) {
   log(Color.hex(hot_pink)(`\nCreate a Fission account and integrate with Webkit.`));

   sh.execSync(`cat ${Path.join(webkitDir, 'publisher/docs/fission/setup.md')} | glow -`, { stdio: 'inherit' })

   let fission = await Inquirer.prompt([{
         type: 'input',
         name: 'username',
         message: 'Fission username:',
         default: publisher.id,
         /* validate: (username) => {
            //? Other username validation rules?
            if (!accounts.includes(username)) return true
            return `That Fission account is already available. (Type: 'nevermind' to return)`
         } */
         //? Apply username validation rules.
      },
      {
         type: 'input',
         name: 'email',
         message: 'Fission email',
         default: publisher.nid
      }
   ])
   let { username, email } = fission
   if (fs.readdirSync(fissionDir).includes(username)) {
      log(Color.yellow(`That Fission account is already available.`))
      return await equipFission(publisher)
   }

   sh.execSync(`fission setup -u ${username} -e ${email}`, { stdio: 'inherit' })
   if (username === fissionLoggedIn()) {
      // Move `~/.config/fission` to `~/.fission/$username`:
      fs.renameSync( Path.join(configDir, 'fission'), Path.join(HOME, `.fission/${username}`) )
      // SymLink active fission account to `~/.config/fission`:
      fs.symlinkSync( Path.join(HOME, `.fission/${username}`), Path.join(configDir, 'fission'))
      // Write account.yaml: { username, password }
      let path = Path.join(HOME, `.fission/${username}/account.yaml`)
      sh.execSync(`yq -P '.' - > ${path}`, { input: JSON.stringify({ username, email }) })
      return { username, email }
   } else return false
}


async function equipFission (publisher) {

   if (!fs.existsSync(fissionDir)) fs.mkdirSync(fissionDir)
   let accounts = fs.readdirSync(fissionDir)
   let account = {}
   //* if no accounts have been previously registered:
   if (!accounts.length) {
      log(Color.blue('\nWe highly recommend that you integrate Webkit with Fission.'))
      sh.execSync(`cat ${Path.join(webkitDir, 'publisher/docs/fission/about.md')} | glow -`, { stdio: 'inherit' })
   
      let { confirm } = await Inquirer.prompt([{
         type: 'confirm',
         name: 'confirm',
         message: 'Integrate Fission?',
         default: true
      }]);
      if (!confirm) {
         log(Color.blue(`  Okay, no worries.\n`))
         return false
      }

      installFission()
      account = await setupFission(publisher)
   } else {
      installFission()

      let { username } = await Inquirer.prompt([{
         type: 'list',
         name: 'username',
         message: "Would you like to use an existing Fission account?",
         choices: [ ...accounts, { name: 'No, setup a new account.', value: 'new' }]
      }])

      if (username === 'new') account = await setupFission(publisher)
      else {
         let email = sh.execSync('cat account.yaml | yq .email -', { cwd: Path.join(HOME, `.fission/${username}`)}).toString().trim() || ''
         account = { username, email }
         fs.symlinkSync( Path.join(HOME, `.fission/${username}`), Path.join(configDir, 'fission'))
      }
   }

   log(Color.green('Fission is all setup!'))
   return account
}

function installFission () {
   let fissionInstalled = false
   try {
      sh.execSync('which fission', { stdio: 'ignore' })
      fissionInstalled = true
   } catch (error) {}

   if (!fissionInstalled) {
      let waiting = Spin('Installing Fission CLI').start();
      sh.execSync(Path.join(webkitDir, 'installer/_install/fission'), { stdio: 'ignore' })
      waiting.stop()
   
      try {
         sh.execSync('which fission', { stdio: 'ignore' })
      } catch (error) {
         log(Color.red(`Fission failed to install. 😵`))
         return false
      }
   }
}




//!async function setupFission (publisher) {}

function fissionLoggedIn () {
   try {
      let username = sh.execSync('fission whoami').toString().split('\n').slice(-2,1)[0].split('\x1B[94m').pop()
      return username
   } catch (error) {
      return false
   }
}


module.exports = { equip }
