#! /bin/node

const log = console.log
const Log = console.log

const fs = require('fs')
const sh = require('child_process')
const Path = require('path');
const Color = require('chalk');
const { hot_pink } = require('../utils/colors.js');
const Debug = require('debug')
const debug = Debug('service');

//const Request = require('axios')
//const OpenPGP = require('openpgp')
const { GPG, CredentialsStore } = require('@webkit.ipfs/security')

const https = require('https')
const Express = require('express')
const Parse = require('body-parser')

const Store = require('nedb');

const env = process.env;
const HOME = env.HOME
const USER = HOME.split('/').pop()

const WEBKIT = { config: Path.join(HOME, '.config/webkit') }
WEBKIT.security = Path.join(WEBKIT.config, 'security.json')


function test (pw=null) {

   let certificates = new CredentialsStore(WEBKIT.security, pw)








}



if (!fs.existsSync(WEBKIT.config)) fs.mkdirSync(WEBKIT.config) //* Bring into Initializaiton?


class Service {
   #api
   #certs
   #policy
   
   /*
   ? Read from: new CredentialsStore(security.json)
      this.#security = {
         key: fs.readFileSync(Path.join(WEBKIT.certs, 'service.key')),
         cert: fs.readFileSync(Path.join(WEBKIT.certs, 'service.cert')),
         requestCert: true,
         rejectUnauthorized: false, //true,
         ca: [ fs.readFileSync(Path.join(WEBKIT.clients, `${USER}.cert.pem`)) ] //Defaults to local USER.
      }
   */

   #publishers
   #activePublisher

   constructor () {
      this.services = null
      this.activePublisher = null

      this.#initialize()
      this.#serveAPI()
      log('constructor')
   }

   #debug (key) {
      debug(`${key}:`)
      debug(this[key])
   }

   async #initialize () {
      this.#certs = new CredentialsStore(WEBKIT.security)

      this.services = readYaml(Path.join(WEBKIT.config, `services.yaml`))
      //? this.services = Configuration.read(path) <== prototype function on Configuration.
      //  Configuration: simple wrapper for reading/writing Yaml files (with or without schema verification)

      this.#publishers = {}


      //!this.#CertificateAuthority()

      this.#API()
      //? this.#debug('services')
   }

   #API () {
      this.#api = Express()
      const { authorize, parseJson } = this.#Middleware()

      this.#api.use(parseJson)
      this.#api.get('/hello', (req, res) => {
         let user = this.activePublisher || 'Webkit Pioneer'

         res.send(`Hello, ${user}!\n`)
      })

      this.#api.post('/login/:id', this.#login.bind(this))
      this.#api.get('/:id/publisher', authorize, (req, res) => {
         let { id } = req.params
         //?res.send(JSON.stringify(this.publishers[id].publisher))
         res.send(this.publishers[id].config)
      })

      this.#api.get('/:id/credential/:key', authorize, this.#readCredential.bind(this))
      this.#api.post('/:id/credential/:key', authorize, this.#writeCredential.bind(this))

      this.#api.get('/:id/password', authorize, (req, res) => {
         let { id } = req.params
         res.send(this.publishers[id].password)
      })

      this.#api.get('/hello/:name', authorize, this.#hello)

      /* const API = Express()
      const { authorize, parseJson } = this.#Middleware()

      API.use(parseJson)
      API.post('login/:id', this.#login.bind(this))
      API.get('/:id/publisher', authorize, (req, res) => {
         let { id } = req.params
         //?res.send(JSON.stringify(this.publishers[id].publisher))
         res.send(this.publishers[id].config)
      })

      API.get('/:id/credential/:key', authorize, this.#readCredential.bind(this))
      API.post('/:id/credential/:key', authorize, this.#writeCredential.bind(this))

      API.get('/:id/password', authorize, (req, res) => {
         let { id } = req.params
         res.send(this.publishers[id].password)
      })

      API.get('/hello/:name', authorize, this.#hello)

      this.#api = API */
   }


   #serveAPI () {
      const port = 9500 //? seek open port.
      const service = "Webkit Login Service"

      const message = () => {
         log(this.#api)
         log(Color.hex(hot_pink)(`${service} is up.\n`) + `https://localhost:${port}`);
      }

      const security = {
         key: fs.readFileSync(Path.join(WEBKIT.certs, 'service.key')),
         cert: fs.readFileSync(Path.join(WEBKIT.certs, 'service.cert')),
         requestCert: true,
         rejectUnauthorized: false, //true,
         ca: [ fs.readFileSync(Path.join(WEBKIT.clients, `${USER}.cert.pem`)) ] //Defaults to local USER.
      }
      https.createServer(security, this.#api).listen(port, message)
      //? process.on('SIGTERM', this.#logout)
   }

   async #generateCertificate () {

   }

   /* async #generateToken () {
      const { privateKey } = await OpenPGP.generateKey({
         userIDs: [{ name: USER, email: `${USER}@webkit.local` }],
         //? Option to write token as 'obect' rather than 'armored'
         format: 'armored'
      });
   
     fs.writeFileSync(Path.join(HOME, `.config/webkit/session.key`), privateKey)
     this.session = privateKey
   } */

   #login (req, res) { // /login/:id
      log('login()')
      let { id } = req.params
      let { nid, pw } = req.body
      //? Get NID from ID.

      if (!publisherExists(id)) return res.send(false)
      if (!GPG.authenticate(nid, pw)) return res.send(false)
      debug(`authenticated: ${id}`)

      fs.mkdirSync(publisherPath(id), { recursive: true })

      let config = readYaml(Path.join(publisherPath(id), 'publisher.yaml'))
      debug(`config: ${config}`)

      this.publishers[id] = {
         password: pw,
         config,
         credentials: new Store({
            filename: Path.join(publisherPath(id), 'credentials.json'),
            autoload: true,
            //?afterSerialization: (data) => data,
            //?beforeDeserialization: (data) => data
            afterSerialization: (data) => GPG.encrypt(data, nid, pw),
            beforeDeserialization: (encrypted) => GPG.decrypt(encrypted, pw)
         })
      }

      //! this.loggedIn = true
      return res.send(true)
   // Request.post(`http://localhost:9500/login/${id}`, { nid, pw })
   }










   

   #logout () {
      let { id } = req.params
      delete this.publishers[id]
      res.send(true)

      /*const seconds = 30
      this.http.close(function () {
         console.log( "Closed out remaining connections.");
         // Close db connections, etc.
         });
      
         setTimeout( function () {
         console.error("Could not close connections in time, forcefully shutting down");
         process.exit(1); 
         }, seconds*1000);
      */

   }


   #hello (req, res) { //next
      let name = req.params.name
      res.send(`Welcome, ${name}!\n`)
      //next();
   }


   async #readCredential (req, res) {
      let { id, key } = req.params
      let pw = this.publishers[id].password

      this.publishers[id].credentials.findOne({ key }, (err, document) => {
         if (err || !document) return res.send(false)

         let { encrypted } = document
         return res.send(GPG.decrypt(encrypted, pw))
      })
   }

   async #writeCredential (req, res) {
      let { id, key } = req.params
      let { data } = req.body
      let nid = readPublisherConfig(id, '.nid')
      let pw = this.publishers[id].password
      let encrypted = GPG.encrypt(data, nid, pw)

      let query = { key }
      let document = { key, encrypted }
      let exists = await this.#credentialExists(id, query)
      if (exists) this.publishers[id].credentials.update(query, document, () => {
         res.send(true)
      })
      else this.publishers[id].credentials.insert(document, () => {
         res.send(true)
      })
   }

   #credentialExists (id, { key }) {
      return new Promise((resolve, reject) => {
         this.publishers[id].credentials.findOne({ key }, (err, document) => {
            if (err) return reject(err)
            if (!document) return resolve(false)
            else resolve(true)
            //!let { encrypted } = document
            //!resolve(GPG.decrypt(encrypted))
         })
      })
   }

   #Middleware () {
      return {
         authorize: (req, res, next) => {
            log('authorize()')
            //? let cert = req.connection.getPeerCertificate()
            //? if (!req.client.authorized) return res.status(401).send("Unauthorized")
            
            let { id } = req.params
            if (Object.keys(this.publishers).includes(id)) {
               log(`authorized: ${id}`)
               next()
            }
            else res.status(401).send("Unauthorized")
         },
      
         parseJson: Parse.json({ type: 'application/json' })
      }
   }
   
}

let service = new Service()

module.exports = Service


function Publishers () {
   let publishersDir = Path.join(WEBKIT.config, 'publishers')
   if (!fs.existsSync(publishersDir)) fs.mkdirSync(publishersDir, { recursive: true })
   return fs.readdirSync(publishersDir)
}

function publisherExists (id) {
   return Publishers().includes(id)
}


function publisherPath (id) {
   return Path.join(WEBKIT.config, `/publishers/${id}`)
}


function readPublisherConfig (id, query=null) {
   let path = Path.join(publisherPath(id), 'publisher.yaml')
   return JSON.parse(sh.execSync(`yq '${query||'.'}' ${path} -o=json`).toString())
}
function writePublisherConfig (id, query=null) {
   let path = Path.join(publisherPath(id), 'publisher.yaml')
   sh.execSync(`yq -P '${query||'.'}' - > ${path}`, { input: JSON.stringify(data) })
}


function readYaml (path, query=null) {
   return JSON.parse(sh.execSync(`yq '${query||'.'}' ${path} -o=json`).toString())
}
function writeYaml(path, data, query=null) {
   sh.execSync(`yq -P '${query||'.'}' - > ${path}`, { input: JSON.stringify(data) })
}





function parentDirectory (dir) {
   let tree = dir.split('/'); tree.pop()
   return tree.join('/')
}








//debug(WEBKIT)

//const ALGORITHM = 'aes-256-cbc'
//const BLOCK_SIZE = 16
//const KEY_SIZE = 32
//const key = crypto.randomBytes(KEY_SIZE)


/* 
Request.post(
   'http://localhost:9500/yarrow/login', 
   { nid: 'yarrow@webkit.ipfs', pw: 'secret' }
).then(res => console.log(res.data))

const Request = require('axios')
Request
   .get(`http://localhost:9500/${id}/publisher`)
   .then(res => console.dir(res.data))
*/



//* Create 'certificates.json' store for CA & clients.
/* 
```
/? Ensure that Certificate Authority is initialized

   * Exists?: 'certificates.json'

   * Exists?: certificates.get('CA.key')
      * Create Certificate Authority
      * ['CA.key','CA.pub','CA.cert']

   * Exists?: /etc/ssl/certs/Webkit.pem
      * Install CA certificate in Trust.
         * (https://ubuntu.com/server/docs/security-trust-store)

/? Ensure that client has been issued client keys/certificate:
   * Exists?: Client Key/Certificate:
      * ['steward.key','steward.pub','steward.cert']

/? Initialize CredentialsStore.

```
*/
