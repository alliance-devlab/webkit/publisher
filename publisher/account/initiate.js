const Inquire = require('inquirer')

const log = console.log
const fs = require('fs')
const sh = require('child_process')
const Path = require('path');
const Color = require('chalk');
const { hot_pink } = require('../utils/colors.js');
const waitForEnter = require('../utils/waitForEnter')

//! const GPG = require('../security/GPG')
const GPG = require('@webkit.ipfs/gpg')
//! const Store = require('nedb')
const equipPublisher = require('./equip').equip

const HOME = process.env.HOME
const USER = HOME.split('/').pop()
const webkitDir = Path.join(fs.realpathSync(process.argv[1]), '../../..')
//? debug(`webkitDir: ${webkitDir}`)

const WEBKIT = {}
WEBKIT.config = Path.join(HOME, '.config/webkit')
if (!fs.existsSync(WEBKIT.config)) fs.mkdirSync(WEBKIT.config) //* Bring into Initializaiton?

const isMasterPublisher = Boolean(fs.readdirSync(Path.join(WEBKIT.config, 'publishers')).length === 0)


// Validation:
const { requireLetterAndNumber } = require('../utils/validate').password
// String transformation:
function capitalize(phrase) {
   let words = phrase.split(' ')
   return words.map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
}



module.exports = async function () {
   log(Color.hex(hot_pink)(`Let's create a new publisher account.`));

   let publisher = await identify()

   log(Color.yellow(`  Welcome to the Web3 frontier, ${publisher.name.split(' ')[0]}\n`))

   let success = await secure(publisher)
   if (!success) {
      //? log(Color.red(`Failed to secure publisher account.`))
      return process.exit()
   }

   //! Remove the following neDB implimentation:
   /* let account = publisher
   delete account.pw

   let dataDir = `${HOME}/.config/webkit/publishers/${publisher.id}/data`
   if (!fs.existsSync(dataDir)) fs.mkdirSync(dataDir)

   const query = (id) => {
      return new Promise((resolve, reject) => {
         db.findOne({ id }, (err, res) => {
            if (err) reject(err)
            if (!res) resolve(false)
            else resolve(res.data)
         })
      })
   }

   //?let db = new Database({ filename: DatabasePath(publisher.id, 'publisher') })
   let db = new Store({ filename: `${dataDir}/publisher.db`, autoload: true })
   
   if (await query(publisher.id)) db.update({ id }, account)
   else db.insert(account) */


   writePublisherConfig(account) //?

   let capabilities = await equipPublisher(publisher)
   log(Color.yellow(`initiatePublisher().capabilities:`))
   console.dir(capabilities)
   if (capabilities) {
      writePublisherConfig({ ...account, capabilities }) //?
      db.update({ ...publisher, capabilities })
   }

   log(Color.green('Your account is equipped!'))
   //* Next up: glow post-initate.md

   //?sh.execSync(`echo '${JSON.stringify(publisher)}' | jq`, { stdio: 'inherit' })
   //? await login(publisher) ?
}



async function identify () {
   let { confirm } = await Inquire.prompt([{
      type: 'confirm',
      name: 'confirm',
      message: 'Create publisher account now?',
      default: true
   }]);
   if (!confirm) {
      log(Color.blue(`  Okay, no worries.\n`))
      process.exit()
   }

   let publisher = await Inquire.prompt([{
         type: 'input',
         name: 'id',
         message: 'Local publisher ID:',
         default: USER,
         //! Make USER lowercase for sure.
         //? Apply username validation rules.
      },
      {
         type: 'input',
         name: 'name',
         message: 'What should we call you?',
         default: capitalize(USER)
      },
      {
         type: 'input',
         name: 'nid',
         message: 'Network identity:',
         default: `${USER}@my.org`,
         //? filter: toLowerCase
      }
   ])

   //?log(Color.magenta('Publisher:'))
   //?console.dir(publisher)

   //! Does publisher account already exist?
   let configDir = Path.join(Path.join(HOME), '.config')
   let publisherConfig = Path.join(configDir, `webkit/publishers/${publisher.id}`)

   if (fs.existsSync(publisherConfig)) {
      if (fs.readdirSync(publisherConfig).length > 0) {
         log(Color.yellow(`\n  Publisher account '${publisher.id}' already exists.`))
         log(Color.dim(`   Run: \`webkit login ${publisher.id}\``))
         process.exit()
         
         //!await waitForEnter().catch(() => process.exit())
         //!return await identify()

      } else { //* Remove empty directory:
         fs.rmdirSync(publisherConfig)
      }
   }

   return publisher
}


async function secure (publisher) {
   let { choice } = await Inquire.prompt([{
      name: 'choice',
      type: 'list',
      message: "🔒 Secure your account.",
      choices: [
         { name: 'Generate new security keys.', value: 'generate' },
         { name: 'Import your existing keys.', value: 'import' },
      ]
   }])

   let success = false
   if (choice === 'generate') success = Boolean(await generatePublisherKeys(publisher))
   else if (choice === 'import') success = Boolean(await importPublisherKeys(publisher))

   if (success) {
      log(Color.magenta.bold.bgWhiteBright(`Keys belonging to '${publisher.nid}' are Ultimately Trusted in GPG Keyring.\n`))
      return true
   } else {
      return false
   }
}


async function generatePublisherKeys (publisher) {
   if (!GPG.keysExist(publisher.nid)) {
      log(Color.hex(hot_pink)(`🔑 Generate GPG keys for your account.`));
      publisher.pw = await setPassword();

      let publisherDir = Path.join(WEBKIT.config, `/publishers/${publisher.id}`)
      fs.mkdirSync(publisherDir, { recursive: true })

      //? if (isMasterPublisher) CertificateAuthority.initialize(publisher)

      log(Color.blue(`  Generating PGP keys for:`))
      log(Color.whiteBright(`   ${publisher.name} <${nid}>\n`))
      let path = Path.join(HOME, `.config/webkit/publishers/${publisher.id}`)
      let key = publisher.nid
      return Boolean(await GPG.generateKeys(path, key, publisher.pw))
   }

   else { //? Keys already exist with that id:
      log(Color.hex(hot_pink)(`\nPublisher keys for '${publisher.nid}' already exist.`))
      let { choice, gpg_pw, method } = await Inquire.prompt([
         {
            name: 'choice',
            type: 'list',
            message: `Use '${publisher.nid}' keys?`,
            choices: [
               { name: 'Yes, use existing keys.', value: 'import' },
               { name: 'Generate new security keys.', value: 'generate' },
            ],
         },
         {
            name: 'gpg_pw',
            type: 'password',
            message: 'Unlock private key:',
            mask: '*',
            when: answers => (answers.choice == 'import')
         },
         {
            name: 'method',
            type: 'list',
            message: 'GPG keys must be uniquely identified.',
            choices: [
               { name: 'Assign different network id to publisher key.', value: 'make_new' },
               { name: 'Destroy the existing keys.', value: 'destroy_old' }
            ],
            when: answers => (answers.choice == 'generate')
         }
      ])


      if (choice === 'import') {
         return Boolean(await importPublisherKeys({ nid, pw: gpg_pw }))
      }
      
      
      else if (choice === 'generate') {
         log(`generate method: ${method}`)
         log(Color.red(`Feature not implimented yet.`))
         return false
      }


      process.exit()
   }
}

async function importPublisherKeys ({ nid, pw }) {
   if (!GPG.keysExist(nid)) return false

   if (!GPG.authenticate(nid, pw)) {
      log(Color.red(`  That passphrase was incorrect.`))
      let success = await tryPassAgain(nid)
      if (!success) return false
   }

   if (!GPG.isUltimatelyTrusted(nid)) {
      log(Color.hex(hot_pink)(`\nPublisher keys must be "ultimately trusted".`));
      let { confirm } = await Inquire.prompt([{
         type: 'confirm',
         name: 'confirm',
         message: `Set '${nid}' keys as `+Color.italic('ultimately trusted')+` in your keyring?`,
         default: true
      }])
      log(``)

      if (confirm) {
         GPG.trustUltimately({ nid, pw })
         if (!GPG.isUltimatelyTrusted(nid)) {
            log(Color.red(`GPG failed to trust '' key ultimately. 😵`))
            return false
         }
      } else return false
   }

   log('')
   warn()
   return true
}


const warn = () => {
   //? This will be the master password, which secures your publisher GPG keys.
   //! Notice: This master password is your responsibility and cannot be recovered.
   //? Webkit is decentralized —You are the authority and sole proprietor of your account data.
   //? For information about account recovery, run: `webkit docs security`
   log(Color.blueBright("  This will be the master password that secures your publisher keys.\n"))
   log(Color.red("  //! Notice:\n  ") + Color.bold(" Your Master Password ") + Color.italic("is your responsibility and cannot be recovered."))
   //?log(Color.whiteBright("   Webkit is decentralized, which means that ") + Color.bold.magenta("You are the authority") + Color.dim.magenta(" and sole proprietor of your account data."))
   log(Color.magenta.dim("   For information about account recovery, run: ") + Color.magenta("`webkit docs security`\n"))
   if (typeof warned !== 'undefined') warned = true
}


const tryPassAgain = async (nid) => {
   let { gpg_pw } = await Inquire.prompt([{
      name: 'gpg_pw',
      type: 'password',
      message: 'Unlock private key:',
      mask: '*'
   }])

   if (GPG.authenticate(nid, gpg_pw)) return true

   log(Color.red(`  That passphrase was incorrect.`))

   let { try_again } = await Inquire.prompt([{
      name: 'try_again',
      type: 'confirm',
      message: 'Try again?',
      default: true
   }])
   if (try_again) return Boolean(await tryPassAgain(nid))
   else return false
}




async function setPassword (warned=false) {

   let { pw } = await Inquire.prompt([{
         type: 'password',
         message: 'Enter password:',
         name: 'pw',
         mask: '*',
         //?validate: requireLetterAndNumber
   }]);

   if (!warned) {
      warn()
      log("ENTER to confirm Notice.\n" + Color.gray("CTRL+C to cancel."))
      await waitForEnter().catch(() => process.exit())
      log(Color.hex(hot_pink)(`\nWith that in mind . . .`));
   }

   if (pw === '') log() //! Are you sure you want a blank password?

   let { pw2 } = await Inquire.prompt([{
      type: 'password',
      message: "Confirm password:",
      name: 'pw2',
      mask: '*',
      //?validate: requireLetterAndNumber
   }])

   if (pw !== pw2) {
      log(Color.red("Those passwords do not match."))
      //? await askQuestion("Try again. ")
      log(Color.hex(hot_pink)(`\nTry again.`));
      return await setPassword(true)
   } 
   
   log(Color.magenta('  Great. ') + Color.magentaBright.italic('Write it down.\n'))
   return pw
}


function writePublisherConfig (publisher) {
   let webkitConfig = Path.join(Path.join(HOME), '.config/webkit')
   let configDir = Path.join(webkitConfig, `/publishers/${publisher.id}`)
   fs.mkdirSync(configDir, { recursive: true })
   writeYaml(Path.join(configDir, `/publisher.yaml`), publisher)
}

function writeYaml(path, json) {
   sh.execSync(`yq -P '.' - > ${path}`, { input: JSON.stringify(json) })
}

