const Store = require('nedb')
const fs = require('fs')
const Path = require('path')
const HOME = process.env.HOME


async function test () {


   let publisher = {
      id: 'steward'
   }

   let dir = Path.join(HOME, `.config/webkit/publishers/${publisher.id}/data`)
   if (!fs.existsSync(dir)) fs.mkdirSync(dir)

   let db = new Store({ filename: Path.join(dir, 'publisher.db'), autoload: true }, (err) => {
      console.log(err)
   })
   db.loadDatabase((err) => {
      console.log(err)
   })

   db.insert(publisher)
   db.findOne({ id: publisher.id }, (err, doc) => {
      console.dir(doc)
   })

   setTimeout(() => {
      console.log('goodbye!')
   }, 2000)

}

test()


/*
const query = (record={}) => {
   return new Promise((resolve, reject) => {
      db.findOne(record, (err, res=null) => {
         console.log(`findOne: ${res ? null : res?.data}`)
         if (err) return reject(err)
         if (!res) return resolve(false)
         else resolve(res.data)
      })
   })
}


async function test ({ db, publisher }) {
   let { id } = publisher
   let exists = await query({ id })
   if (exists) db.update({ id }, publisher)
   else db.insert(publisher)   
}

test({ db, publisher })
*/
