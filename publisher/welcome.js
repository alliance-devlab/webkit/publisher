const fs = require('fs');
const log = console.log
const Path = require('path');
const Color = require('chalk');
const Prompt = require('inquirer').prompt;

const initiatePublisher = require('./auth/initiate')


initializeContext()
welcome()



async function welcome () {

   log(Color.yellowBright.bold("Welcome to Webkit\n"))

   let { choice } = await Prompt([{
      name: 'choice',
      type: 'list',
      message: "Let's get going.",
      choices: [
         { name: 'Create a new publisher account.', value: 'create' },
         { name: 'Recover publisher account.', value: 'recover' },
      ],
   }])

   //log(`choice: ${choice}`)
   log('')

   if (choice == 'create') {
      initiatePublisher()
   }

   if (choice === 'recover') {
      log(Color.red("This feature has not been implimented yet."))
      process.exit()
   }
}


function initializeContext () {
   const mkDir = (dir) => {
      if (! fs.existsSync(dir)) {
         fs.mkdirSync(dir)
      }
   }

   let home = process.env.HOME
   let config = Path.join(home, '.config')
   mkDir(config)
   mkDir(Path.join(config, 'publisher'))
   mkDir(Path.join(config, 'sites'))
}
